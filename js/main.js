;(function () {
	
	'use strict';



	// iPad and iPod detection	
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) || 
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	var fullHeight = function() {
		if ( !isiPad() && !isiPhone() ) {
			$('.js-fullheight').css('height', $(window).height());
			$(window).resize(function(){
				$('.js-fullheight').css('height', $(window).height());
			})
		}
		

	};

	var sliderMain = function() {
		
	  	$('#fh5co-home .flexslider').flexslider({
			animation: "fade",
			slideshowSpeed: 5000
	  	});

	  	$('#fh5co-home .flexslider .slides > li').css('height', $(window).height());	
	  	$(window).resize(function(){
	  		$('#fh5co-home .flexslider .slides > li').css('height', $(window).height());	
	  	});

	};

	var sliderSayings = function() {
		$('#fh5co-sayings .flexslider').flexslider({
			animation: "slide",
			slideshowSpeed: 5000,
			directionNav: false,
			controlNav: true,
			smoothHeight: false,
			reverse: true
	  	});
	}

	var offcanvasMenu = function() {
		$('body').prepend('<div id="fh5co-offcanvas" />');
		$('body').prepend('<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>');

		$('.fh5co-main-nav .fh5co-menu-1 a, .fh5co-main-nav .fh5co-menu-2 a').each(function(){

			var $this = $(this);

			$('#fh5co-offcanvas').append($this.clone());

		});
		// $('#fh5co-offcanvas').append
	};

	var mainMenuSticky = function() {
		
		var sticky = $('.js-sticky');

		sticky.css('height', sticky.height());
		$(window).resize(function(){
			sticky.css('height', sticky.height());
		});

		var $section = $('.fh5co-main-nav');
		
		$section.waypoint(function(direction) {
		  	
		  	if (direction === 'down') {

			    	$section.css({
			    		'position' : 'fixed',
			    		'top' : 0,
			    		'width' : '100%',
			    		'z-index' : 99999
			    	}).addClass('fh5co-shadow');;

			}

		}, {
	  		offset: '0px'
		});

		$('.js-sticky').waypoint(function(direction) {
		  	if (direction === 'up') {
		    	$section.attr('style', '').removeClass('fh5co-shadow');
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 69; }
		});

	};
	
	// Parallax
	var parallax = function() {

		$(window).stellar();

	};


	// Burger Menu
	var burgerMenu = function() {

		$('body').on('click', '.js-fh5co-nav-toggle', function(event){

			var $this = $(this);

			$('body').toggleClass('fh5co-overflow offcanvas-visible');
			$this.toggleClass('active');
			event.preventDefault();

		});

	};

	var scrolledWindow = function() {

		$(window).scroll(function(){

			var scrollPos = $(this).scrollTop();


			$('#fh5co-home .fh5co-text').css({
		      'opacity' : 1-(scrollPos/300),
		      'margin-top' : (-212) + (scrollPos/1)
		   });

		   $('#fh5co-home .flexslider .fh5co-overlay').css({
				'opacity' : (.5)+(scrollPos/2000)
		   });

		   if (scrollPos > 300) {
				$('#fh5co-home .fh5co-text').css('display', 'none');
			} else {
				$('#fh5co-home .fh5co-text').css('display', 'block');
			}
		   

		});

		$(window).resize(function() {
			if ( $('body').hasClass('offcanvas-visible') ) {
		   	$('body').removeClass('offcanvas-visible');
		   	$('.js-fh5co-nav-toggle').removeClass('active');
		   }
		});
		
	};


	var goToTop = function() {

		$('.js-gotop').on('click', function(event){
			
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500);
			
			return false;
		});
	
	};


	// Page Nav
	var clickMenu = function() {
		var topVal = ( $(window).width() < 769 ) ? 0 : 58;

		$(window).resize(function(){
			topVal = ( $(window).width() < 769 ) ? 0 : 58;		
		});
		$('.fh5co-main-nav a:not([class="external"]), #fh5co-offcanvas a:not([class="external"])').click(function(event){
			var section = $(this).data('nav-section');
			var local = $(this).data('section-id');

			if(local == 1) {
				if ( $('div[data-section="' + section + '"]').length ) {

					$('html, body').animate({
			        	scrollTop: $('div[data-section="' + section + '"]').offset().top - 100
			    	}, 500);	
			    	
			   }
			} else {
				if ( $('div[data-section="' + section + '"]').length ) {

					$('html, body').animate({
			        	scrollTop: $('div[data-section="' + section + '"]').offset().top - topVal
			    	}, 500);	
			    	
			   }
			}

				

		    event.preventDefault();

		    // return false;
		});


	};

	// Reflect scrolling in navigation
	var navActive = function(section) {
		
		$('.fh5co-main-nav a[data-nav-section], #fh5co-offcanvas a[data-nav-section]').removeClass('active');
		$('.fh5co-main-nav, #fh5co-offcanvas').find('a[data-nav-section="'+section+'"]').addClass('active');
		
	};

	var navigationSection = function() {

		var $section = $('div[data-section]');
		
		$section.waypoint(function(direction) {
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}

		}, {
	  		offset: '150px'
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 155; }
		});

	};


	// Animations
	var homeAnimate = function() {
		if ( $('#fh5co-home').length > 0 ) {	

			$('#fh5co-home').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						$('#fh5co-home .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};



	var aboutAnimate = function() {
		var about = $('#fh5co-about');
		if ( about.length > 0 ) {	

			about.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						about.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						about.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var sayingsAnimate = function() {
		var sayings = $('#fh5co-sayings');
		if ( sayings.length > 0 ) {	

			sayings.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						sayings.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);


					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var featureAnimate = function() {
		var feature = $('#fh5co-featured');
		if ( feature.length > 0 ) {	

			feature.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						feature.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						feature.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('bounceIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 500);


					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var typeAnimate = function() {
		var type = $('#fh5co-type');
		if ( type.length > 0 ) {	

			type.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						type.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var foodMenusAnimate = function() {
		var menus = $('#fh5co-menus');
		if ( menus.length > 0 ) {	

			menus.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						menus.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						menus.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 500);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};


	var eventsAnimate = function() {
		var events = $('#fh5co-events');
		if ( events.length > 0 ) {	

			events.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						events.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						events.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 500);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var reservationAnimate = function() {
		var contact = $('#fh5co-contact');
		if ( contact.length > 0 ) {	

			contact.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						contact.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						contact.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 500);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var footerAnimate = function() {
		var footer = $('#fh5co-footer');
		if ( footer.length > 0 ) {	

			footer.waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {


					setTimeout(function() {
						footer.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 200);

					setTimeout(function() {
						footer.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
							
						});
					}, 500);

					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );

		}
	};

	var clickAcrescimos = function() {
		$('.turbine').on('click',function(){
			$(this).parent().find('.box-acrescimos').slideToggle();
		});
	}

	var facebook = function(){
		window.fbAsyncInit = function () {
	        FB.init({
	            appId: '1839987716058375',
	            cookie: true,
	            xfbml: true,
	            version: 'v3.0'
	        });
	    };
	    (function (d, s, id) {
	        var js, fjs = d.getElementsByTagName(s)[0];
	        if (d.getElementById(id)) return;
	        js = d.createElement(s); js.id = id;
	        js.src = "//connect.facebook.net/pt_BR/sdk.js";
	        fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));

	    $("#btnStart").click(function () {
	        FB.login(function (response) {
	            if (response.status === 'connected') {               
	                FB.api('/me?fields=id,name,email', function (fb) {
	                    var User = {};
	                    User.ID_Facebook = fb.id;
	                    User.Name = fb.name;
	                    User.Email = fb.email;

	                    $('.nome').val(User.Name);
	                    $('.email').val(User.Email);
	                    $('#btnStart, .msg-face').hide();
	                    $('.login-ok').show();
	                });
	            }

	        }, { scope: 'public_profile,email' });
	    })

	}

	var calculateCheckbox = function() {
		$('.box-acrescimos input[type="checkbox"]:checkbox').click(function() {
		  var total = $(this).closest('.produto').find('span').html();
		  var valor = $(this).val();
		  if ($(this).is(":checked")) {
		    total = (parseFloat(total) + parseFloat(valor)).toFixed(2);
		  } else {
		    total = (parseFloat(total) - parseFloat(valor)).toFixed(2);
		  }
		  total = $(this).closest('.produto').find('.preco span').html(total);
		});
		
	}
	var valorTotalCarrinho = 0;

	var addCart = function() {
		$('.adicionar-carrinho').click(function() {
		  var $this = $(this);
		  var closest = $(this).closest('.produto');
		  var produto = $(this).closest('.produto').find('.item h2').html();
		  var valorproduto = $(this).closest('.produto').data('preco');
		  var valortotal = $(this).closest('.produto').find('.preco span').html();
		  var valorCarrinhoProduto = $(this).closest('.produto').find('.preco span').html();
		  $('.incluido-no-carrinho').addClass('active');

		  // Incluir no carrinho
		  var checks = closest.find("input[type='checkbox']:checked"),
		  i = checks.length,
		  acrescimos = [];

		  while (i--) {
		  	acrescimos.push(checks[i].getAttribute('data-nome'));
		  }


		  var pedido = "<tr><td><input type='hidden' name='produto[]' value='"+ produto + " - " + acrescimos + "'>" + produto + "<br/> <small>"  + acrescimos + "</small></td><td class='valor-carrinho'><input type='hidden' name='valor[]' value='"+ valortotal +"'>" + "R$ <span>" + valortotal + "</span></td> <td><a href='javascript:;' class='remover'>X</a></td></tr>";

		  var soma = (parseFloat($('.valor-total-carrinho').find('span').html()) + parseFloat(valortotal));

		  $('#pedido').append(pedido);
		  $('.nenhum-produto').remove();
		  
		  var sum = 0.00;
		  $('.valor-carrinho span').each(function(){
		  	sum += Number($(this).html());
		  	$('.valor-total-carrinho span').html(sum.toFixed(2));
		  	$('.valor-total-carrinho input').val(sum.toFixed(2));
		  });

		  var totalEntrega = $('.valor-total-entrega span').html();
		  var valorTotalCarrinho = $('.valor-total-carrinho span').html()

		  var totalGeral = (parseFloat(valorTotalCarrinho) + parseFloat(totalEntrega));

		  $('.valor-total-pedido span').html(totalGeral.toFixed(2))
		  $('.valor-total-pedido input').val(totalGeral.toFixed(2))


		  setTimeout(function() {
		  	$('.incluido-no-carrinho').removeClass('active');
		  	
		  	closest.find("input:checkbox").removeAttr('checked');
		  	closest.removeAttr('checked');
		  	closest.find('.preco span').html(valorproduto);
		  	closest.find('.box-acrescimos').slideUp();
		  }, 2000);
		});
		
	}

	var removeCart = function() {
		$(document).on('click', '.remover', function(){
			var subtracao = (parseFloat($('.valor-total-carrinho span').html()) - parseFloat($(this).closest('tr').find('.valor-carrinho span').html()));
			$(this).closest('tr').remove();
			$('.valor-total-carrinho span').html(subtracao.toFixed(2));
			$('.valor-total-carrinho input').val(subtracao.toFixed(2));
			var subtracaoTotal = (parseFloat($('.valor-total-carrinho span').html() + parseFloat($('.valor-total-entrega span').html())));
			$('.valor-total-pedido span').html(subtracaoTotal.toFixed(2));
			$('.valor-total-pedido input').val(subtracaoTotal.toFixed(2));
			var somatudo2 = (parseFloat($('.valor-total-carrinho span').html()) + parseFloat($('.valor-total-entrega span').html()));
			$('.valor-total-pedido span').html(somatudo2.toFixed(2));
			$('.valor-total-pedido input').val(somatudo2.toFixed(2));
		})
	}

	var entregaCart = function() {
		$('.teleentrega').change(function(){
			var $this = $(this);
			var bairrolocal = $this.find('option:selected').text();
			var val = parseFloat($this.val());


			$('.bairro-local').val(bairrolocal);

			$('.valor-total-entrega span').html(val.toFixed(2));
			$('.valor-de-entrega').val(val.toFixed(2));

			var somatudo = (parseFloat($('.valor-total-carrinho span').html()) + parseFloat($('.valor-total-entrega span').html()));
			$('.valor-total-pedido span').html(somatudo.toFixed(2));
			$('.valor-total-pedido input').val(somatudo.toFixed(2));
			
		});
	}


	// Document on load.
	$(function(){

		fullHeight();
		sliderMain();
		sliderSayings();
		offcanvasMenu();
		mainMenuSticky();
		parallax();
		burgerMenu();
		scrolledWindow();
		clickMenu();
		navigationSection();
		goToTop();

		$(".telefone").mask("(99) 99999-9999"); 
		$(".cep").mask("99999-999"); 



		// Animations
		homeAnimate();
		aboutAnimate();
		sayingsAnimate();
		featureAnimate();
		typeAnimate();
		foodMenusAnimate();
		eventsAnimate();
		reservationAnimate();
		footerAnimate();
		clickAcrescimos();
		calculateCheckbox();
		addCart();
		removeCart();
		entregaCart();
		facebook();
	});


}());