<?php include "../_conexao/conexao.php"; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Komilaw, Buffet e Pastelaria</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="A melhor pastelaria de Pelotas"/>
	<meta name="keywords" content="melhor pastel de pelotas, pastel, pelotas, rs, pastelaria em pelotas, pastelaria pelotas, pasteis, buffet pelotas, melhor buffet em pelotas, viandas pelotas, marmitas pelotas, ala minuta pelotas, qualidade, webhnet" />
	<meta name="author" content="webhnet - oficina web" />


  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="Cardápio - Komil'ãw Pastelaria"/>
	<meta property="og:image" content="../images/facebook.jpg"/>
	<meta property="og:url" content="http://komilaw.com.br/pedidos/"/>
	<meta property="og:site_name" content="Komilaw, Buffet e Pastelaria"/>
	<meta property="og:description" content="O melhor Restaurante e Pastelaria da cidade"/>
	<meta name="twitter:title" content="Komilaw, Buffet e Pastelaria" />
	<meta name="twitter:image" content="../images/facebook.jpg" />
	<meta name="twitter:url" content="http://komilaw.com.br/pedidos/" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="../favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">


	<!-- Animate.css -->
	<link rel="stylesheet" href="../css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="../css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="../css/simple-line-icons.css">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="../css/flexslider.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="../css/bootstrap.css">

	<link rel="stylesheet" href="../css/style.css">


	<!-- Modernizr JS -->
	<script src="../js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body class="pedidos">
	<div class="incluido-no-carrinho">
		<i class="fas fa-check-circle"></i> Adicionado no carrinho! Aguarde...
	</div>


	<?php
        /** Salva Pedido */

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $txtPedito = '';

        if (!empty($_POST['produto']) && is_array($_POST['produto'])) {
            foreach ($_POST['produto'] as $key => $value) {
                $txtPedito .= $key + 1 . ' - ' . strip_tags($value) . ' R$' . $_POST['valor'][$key] . PHP_EOL;
            }
        }

        $nome     			= @utf8_decode($_POST['name']);
        $email    			= @$_POST['email'];
        $telefone 			= @$_POST['telefone'];
        $endereco 			= @utf8_decode($_POST['endereco']);
        $pagamento 			= @utf8_decode($_POST['pagamento']);
        $teleentrega 		= @utf8_decode($_POST['bairro']);
        $observacao 		= @utf8_decode($_POST['observacao']);
        $valorProdutos 		= @$_POST['valor-de-produtos'];
        $valorEntrega 		= @$_POST['valor-de-entrega'];
        $valorPedido 		= @$_POST['valor-total-carrinho'];
        $status 			= 0;
        $criadoEm = date('d-m-Y H:i:s');

        $query = "INSERT INTO `pedidos`
                        (`pedido_nome`,
                        `pedido_email`,
                        `pedido_telefone`,
                        `pedido_endereco`,
                        `pedido_bairro`,
                        `pedido_observacao`,
                        `pedido_valor_de_produtos`,
                        `pedido_valor_de_entrega`,
                        `pedido_valor_total`,
                        `pedido_status`,
                        `pedido_pedido`,
                        `pedido_data`)
                        VALUES (
                        '$nome',
                        '$email',
                        '$telefone',
                        '$endereco',
                        '$teleentrega',
                        '$observacao',
                        '$valorProdutos',
                        '$valorEntrega',
                        '$valorPedido',
                        '$status',
                        '$txtPedito',
                        '$criadoEm'
                        );";

            $rst = mysql_query($query) or die(mysql_error());
        }

        function getSlug($catName)
		{
			return strtolower( ereg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim(utf8_encode($catName))), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")));
		}

		function getAcrescimos($produtoId)
		{
			$queryAcrescimos = "
				SELECT *
				  FROM con_acrescimo_produtos AS cap
				  JOIN acrescimos AS a
					ON a.acrescimo_id = cap.con_acrescimo_id
				 WHERE cap.con_produto_id = $produtoId
				 ORDER BY a.acrescimo_nome;
			";

			$resultAcrescimos = mysql_query($queryAcrescimos);

			$aAcrescimos = [];
			while ($acrescimo = mysql_fetch_array($resultAcrescimos)) {
				$aAcrescimos[] = [
					'id'   		   => $acrescimo['acrescimo_id'],
					'nome' 		   => $acrescimo['acrescimo_nome'],
					'valorNormal'  => $acrescimo['acrescimo_valor_normal'],
					'valorKomilaw' => $acrescimo['acrescimo_valor_komilaw']
				];
			}

			return $aAcrescimos;
		}

		function getProdutos()
		{
			$queryProdutos = "
			SELECT cp.id, cp.nome as produtoNome, 
				cp.descricao, 
			    cp.preco_normal, 
			    cp.preco_komilaw, 
			    cp.preco_aperitivo, 
			    cat.id as catId, 
			    cat.nome as catNome
			  FROM cardapio_pastelaria AS cp
			  JOIN categoria_produtos as cat
				ON cat.id = cp.cat_id
			 ORDER BY cat.nome, cp.nome
			";
			$resultProduto = mysql_query($queryProdutos);

			$aProdutos = [];
			while ($produto = mysql_fetch_array($resultProduto)) {
				$aProdutos[getSlug($produto['catNome'])][] = [
					'id'   			 => $produto['id'],
					'nome' 			 => utf8_encode(nl2br($produto['produtoNome'])),
					'descricao'		 => utf8_encode(nl2br($produto['descricao'])),
					'precoNormal' 	 => $produto['preco_normal'],
					'precoKomilaw'   => $produto['preco_komilaw'],
					'precoAperitivo' => $produto['preco_aperitivo'],
					'acrescimos'	 => getAcrescimos($produto['id'])
				];
			}

			return $aProdutos;
		}

		$aProdutos = getProdutos();
	?>

	<div id="fh5co-container" style="position:fixed !important; width:100%; left: 0; top:0;">		
		<div class="js-sticky">
			<div class="fh5co-main-nav">
				<div class="container-fluid">
					<div class="fh5co-logo" style="width:10%;">
						<a href="index.html">
							<img src="../images/logo.png" class="logo-menu">
						</a>
					</div>
					<div class="fh5co-menu-1" style="width: 90%;">
						<?php
							$consulta = "SELECT * FROM categoria_produtos order by id ASC";
				            $rst = mysql_query($consulta);
				            while($linha = mysql_fetch_array($rst)) {
				                $categoria_produtos_id  = utf8_encode(nl2br($linha['id']));
				                $categoria_produtos_nome  = utf8_encode(nl2br($linha['nome']));

				                $slug = getSlug($categoria_produtos_nome);
						?>						
							<a href="#" data-section-id="1" data-nav-section="<?=$slug?>">
								<?php echo $categoria_produtos_nome; ?>
							</a>
						<? } ?>
						<a href="#" data-nav-section="finalizar-pedido">
							<strong>Finalizar pedido</strong>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

		

	<div id="all" class="fh5co-featured" style="margin-top: 150px;">
		<div class="container">
			<div class="row text-center fh5co-heading row-padded">
				<div class="col-md-12" data-section="pasta-is-tradicionais">
					<p style="color:#ccc; margin-bottom: 0px;">Horário para pedidos: Das 16h - 23h - Segunda a sábado. </p>
				</div> 
			<?php if (sizeof($aProdutos['pasteis-tradicionais']) > 0): ?>
				<div class="col-md-12" data-section="pasta-is-tradicionais">
					<h2 class="heading to-animate fadeInUp animated">Pastéis tradicionais</h2>
				</div>
				<?php foreach ($aProdutos['pasteis-tradicionais'] as $keyProduto => $produto): ?>
				<?php if (!empty($produto['precoAperitivo']) && $produto['precoAperitivo'] > 0) : ?>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoAperitivo'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Aperitivo <sup>(8cm x 5cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoAperitivo'] ?></span>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoNormal'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Normal <sup>(17cm x 10cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoNormal'] ?></span>
						</div>
						<div class="box-acrescimos">
						<?php foreach ($produto['acrescimos'] as $keyAcrescimo => $acrescimo): ?>
							<div class="checkbox">
								<input type="checkbox" value="<?php echo $acrescimo['valorNormal']?>" name="acrescimo[]" data-nome=" <?php echo $acrescimo['nome'] ?>">
								<?php echo $acrescimo['nome'] ?> - <span>R$<?php echo $acrescimo['valorNormal']?></span>
							</div>
						<?php endforeach ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoKomilaw'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Komilaw <sup>(21cm x 13cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoKomilaw'] ?></span>
						</div>
						<div class="box-acrescimos">
						<?php foreach ($produto['acrescimos'] as $keyAcrescimo => $acrescimo): ?>
							<div class="checkbox">
								<input type="checkbox" value="<?php echo $acrescimo['valorKomilaw']?>" name="acrescimo[]" data-nome=" <?php echo $acrescimo['nome'] ?>">
								<?php echo $acrescimo['nome'] ?> - R$<?php echo $acrescimo['valorKomilaw']?>
							</div>
						<?php endforeach ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			<?php endif ?>
			</div>

			<div class="row text-center fh5co-heading row-padded">
			<?php if (sizeof($aProdutos['pasteis-especiais']) > 0): ?>
				<div class="col-md-12" data-section="pasta-is-especiais">
					<h2 class="heading to-animate fadeInUp animated">Pastéis especiais</h2>
				</div>
				<?php foreach ($aProdutos['pasteis-especiais'] as $keyProduto => $produto): ?>
					<?php if (!empty($produto['precoAperitivo']) && $produto['precoAperitivo'] > 0) : ?>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoAperitivo'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Aperitivo <sup>(8cm x 5cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoAperitivo'] ?></span>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoNormal'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Normal <sup>(17cm x 10cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoNormal'] ?></span>
						</div>
						<div class="box-acrescimos">
						<?php foreach ($produto['acrescimos'] as $keyAcrescimo => $acrescimo): ?>
							<div class="checkbox">
								<input type="checkbox" value="<?php echo $acrescimo['valorNormal']?>" name="acrescimo[]" data-nome=" <?php echo $acrescimo['nome'] ?>">
								<?php echo $acrescimo['nome'] ?> - <span>R$<?php echo $acrescimo['valorNormal']?></span>
							</div>
						<?php endforeach ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="produto clearfix" data-preco="<?php echo $produto['precoKomilaw'] ?>">
						<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
						<?php if (sizeof($produto['acrescimos'])): ?>
						<a href="javascript:;" class="turbine">Turbine +</a>
						<?php endif; ?>
						<div class="item">
							<h2><?php echo @$produto['nome'] . ' - Komilaw <sup>(21cm x 13cm)</sup>' ?></h2>
							<p><?php echo @$produto['descricao'] ?></p>
						</div>
						<div class="preco">
							R$<span><?php echo $produto['precoKomilaw'] ?></span>
						</div>
						<div class="box-acrescimos">
						<?php foreach ($produto['acrescimos'] as $keyAcrescimo => $acrescimo): ?>
							<div class="checkbox">
								<input type="checkbox" value="<?php echo $acrescimo['valorKomilaw']?>" name="acrescimo[]" data-nome=" <?php echo $acrescimo['nome'] ?>">
								<?php echo $acrescimo['nome'] ?> - R$<?php echo $acrescimo['valorKomilaw']?>
							</div>
						<?php endforeach ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			<?php endif ?>
			</div>

			<div class="row text-center fh5co-heading row-padded">
				<?php if (sizeof($aProdutos['pasteis-doces']) > 0): ?>
					<div class="col-md-12" data-section="pasta-is-doces" >
						<h2 class="heading to-animate fadeInUp animated">Pastéis doces</h2>
					</div>
					<?php foreach ($aProdutos['pasteis-doces'] as $keyProduto => $produto): ?>
						<div class="row">
						<div class="col-md-6">
							<div class="produto clearfix" data-preco="<?php echo $produto['valorNormal'] ?>">
								<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
                                <?php if (sizeof($produto['acrescimos'])): ?>
                                    <a href="javascript:;" class="turbine">Turbine +</a>
                                <?php endif; ?>
								<div class="item">
									<h2><?php echo @$produto['nome'] . ' - Normal <sup>(17cm x 10cm)</sup>' ?></h2>
									<p><?php echo @$produto['descricao'] ?></p>
								</div>
								<div class="preco">
									R$<span><?php echo $produto['precoNormal'] ?></span>
								</div>
								<div class="box-acrescimos">
								<?php foreach ($produto['acrescimos'] as $keyAcrescimo => $acrescimo): ?>
									<div class="checkbox">
										<input type="checkbox" data-nome=" <?php echo $acrescimo['nome'] ?>" value="<?php echo $acrescimo['valorNormal']?>" name="acrescimo[]">
										<?php echo $acrescimo['nome'] ?> - R$<?php echo $acrescimo['valorNormal']?>
									</div>
								<?php endforeach ?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="produto clearfix" data-preco="<?php echo $produto['precoAperitivo'] ?>">
								<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
                                <?php if (sizeof($produto['acrescimos'])): ?>
                                    <a href="javascript:;" class="turbine">Turbine +</a>
                                <?php endif; ?>
								<div class="item">
									<h2><?php echo @$produto['nome'] . ' - Aperitivo <sup>(8cm x 5cm)</sup>' ?></h2>
									<p><?php echo @$produto['descricao'] ?></p>
								</div>
								<div class="preco">
									R$<span><?php echo $produto['precoAperitivo'] ?></span>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				<?php endif ?>
			</div>

			<div class="row text-center fh5co-heading row-padded">
				<?php if (sizeof($aProdutos['outros']) > 0): ?>
				<div class="col-md-12"  data-section="outros">
					<h2 class="heading to-animate fadeInUp animated">Outros</h2>
				</div>
				
				<?php foreach ($aProdutos['outros'] as $keyProduto => $produto): ?>	
					<div class="col-md-6">		
						<div class="produto bebidas" data-preco="<?php echo $produto['precoNormal'] ?>">
							<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
							
							<div class="item">
								<h2><?php echo $produto['nome'] ?></h2>
							</div>
							<div class="preco">
								R$<span><?php echo $produto['precoNormal'] ?></span>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				<?php endif ?>
			</div>

			<div class="row text-center fh5co-heading row-padded">
				<?php if (sizeof($aProdutos['bebidas']) > 0): ?>
				<div class="col-md-12"  data-section="bebidas">
					<h2 class="heading to-animate fadeInUp animated">Bebidas</h2>
				</div>
				
				<?php foreach ($aProdutos['bebidas'] as $keyProduto => $produto): ?>	
					<div class="col-md-6">		
						<div class="produto bebidas" data-preco="<?php echo $produto['precoNormal'] ?>">
							<a href="javascript:;" class="adicionar-carrinho"><i class="fas fa-cart-plus"></i></a>
							
							<div class="item">
								<h2><?php echo $produto['nome'] ?></h2>
							</div>
							<div class="preco">
								R$<span><?php echo $produto['precoNormal'] ?></span>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				<?php endif ?>
			</div>

			<div class="row text-center fh5co-heading row-padded">
				<div class="col-md-12"  data-section="finalizar-pedido">
					<h2 class="heading to-animate fadeInUp animated">Finalizar pedido</h2>
					<p class="sub-heading msg-face" style="color:white;">Estamos quase lá, só precisamos que você faça seu login no facebook e finalize seu pedido :).</p>
					<p class="login-ok" style="color:white; display: none;">Parabéns já temos seus dados, agora confirme seu pedido e nos envie.</p>
					<small class="msg-face" style="color: white;">Para sua segurança e a nossa é obrigatório fazer seu login com o facebook.</small>
				</div>
			</div>
			<a href="javascript:;" id="btnStart" style="width: 100%; text-align:center; "><img src="../images/botao-facebook.png" style="width: 320px; display: block; margin:0 auto 40px;"></a>
			<form class="row text-left fh5co-heading row-padded form-pedido" method="POST">
				<div class="col-md-4">
					<label>Seu nome</label>
					<input type="text" disabled value="" name="name-fake" class="nome">
					<input type="hidden" value="" name="name" class="nome" required>
				</div>
				<div class="col-md-4">
					<label>Seu email</label>
					<input type="text" disabled value="" name="email-fake" class="email">
					<input type="hidden" value="" name="email" class="email" required>
				</div>
				<div class="col-md-4">
					<label>Seu telefone</label>
					<input type="text" value="" name="telefone" class="telefone" required>
				</div>
				<div class="col-md-4">
					<label>Seu endereço</label>
					<input type="text" value="" name="endereco" class="endereco" required>
				</div>
				<div class="col-md-4">
					<label>Forma de pagamento?</label>
					<select name="pagamento">
						<option value="">Selecione</option>
						<option value="Dinheiro">Dinheiro</option>
						<option value="Cartão - Visa">Cartão - Visa</option>
						<option value="Cartão - Master">Cartão - Master</option>
						<option value="Cartão - Hipercard">Cartão - Hipercard</option>
						<option value="Cartão - Elo">Cartão Crédito - Elo</option>
						<option value="Cartão Débito - Banrisul ">Cartão Débito - Banrisul</option>
					</select>
				</div> 
				<div class="col-md-4">
					<label>Bairro - Tele-entrega</label>
					<select name="teleentrega_valor" required class="teleentrega">
						<option value="">Selecione seu bairro</option>
						<option value="0.00" rel="Retirar no local">Retirar no estabelecimento - GRÁTIS</option>
						<?php
							$consulta = "SELECT * FROM cardapio_frete order by cardapio_frete_id ASC";
				            $rst = mysql_query($consulta);
				            while($linha = mysql_fetch_array($rst)) {
				                $frete_nome  = utf8_encode($linha['cardapio_frete_bairro']);
				                $frete_valor  = $linha['cardapio_frete_valor'];
						?>
						<option value="<?php echo $frete_valor ?>" rel="<?php echo $frete_nome." - R$".$frete_valor ?>"><?php echo $frete_nome." - R$".$frete_valor ?></option>
						<? } ?>
					</select>
					<input name="bairro" class="bairro-local" type="hidden" value="">
				</div>
				<div class="col-md-12">
					<table class="table table-bordered" style="margin: 15px 0;">
				    <thead>
				      <tr>
				        <th>Seu Pedido</th>
				        <th>Valor</th>
				        <th>Remover</th>
				      </tr>
				    </thead>
				    <tbody id="pedido">
				       <tr class="nenhum-produto">
				       		<td>Nenhum produto</td>
				       		<td>-</td>
				       </tr>
				    </tbody>
				    <tfooter>
				    	<tr>
				    		<td></td>
				    		<td>Valor produtos:</td>
				    		<td  class="valor-total-carrinho">
				    			R$ <span>0.00</span>
				    			<input type="hidden" name="valor-de-produtos" value="">
				    		</td>
				    	</tr>
				    	<tr>
				    		<td></td>
				    		<td>Valor entrega:</td>
				    		<td class="valor-total-entrega">
				    			R$ <span>0.00</span>
				    			<input type="hidden" name="valor-de-entrega" value="" class="valor-de-entrega">
				    		</td>
				    	</tr>
				    	<tr>
				    		<td></td>
				    		<td>Valor total:</td>
				    		<td class="valor-total-pedido">
				    			R$ <span>0.00</span>
				    			<input type="hidden" name="valor-total-carrinho" value="">
				    		</td>
				    	</tr>
				    </tfooter>
				  </table>
				</div>
				<div class="col-md-12">
					<textarea name="observacao" placeholder="Alguma observação sobre seu pedido?"></textarea>
				</div>
				<div class="col-md-4 col-md-offset-8" style="margin-top:15px;">
					<div class="form-group ">
						<input class="btn btn-primary" style="color:white" value="ENVIAR MEU PEDIDO" type="submit">
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>

	<div id="fh5co-footer">
		<div class="container">
			<div class="row row-padded">
				<div class="col-md-12 text-center">
					<p class="to-animate">2018 Komil'aw Buffet e Pastelaria. <br> Desenvolvido por <a href="http://webhnet.com.br" target="_blank">Webhnet Oficina Web & Design</a>
					</p>
					<p class="text-center to-animate"><a href="#" class="js-gotop">Voltar ao topo</a></p>
				</div>
			</div>
		</div>
	</div>


	
	
	
	
	<!-- jQuery -->
	<script src="../js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="../js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="../js/bootstrap.min.js"></script>
	<!-- Bootstrap DateTimePicker -->
	<script src="../js/moment.js"></script>
	<script src="../js/bootstrap-datetimepicker.min.js"></script>
	<!-- Waypoints -->
	<script src="../js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="../js/jquery.stellar.min.js"></script>

	<!-- Flexslider -->
	<script src="../js/jquery.flexslider-min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script>
		$(function () {
	       $('#date').datetimepicker();
	   });
	</script>
	<!-- Main JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

	<script src="../js/main.js"></script>

	</body>
</html>

