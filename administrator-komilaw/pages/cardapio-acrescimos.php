<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">CARDÁPIO PASTELARIA > ACRESCIMOS</h1>
                </div>
            </div>
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST"){

                    $acrescimo_nome             = utf8_decode($_POST['acrescimo_nome']);
                    $acrescimo_valor_normal     = utf8_decode($_POST['acrescimo_valor_normal']);
                    $acrescimo_valor_komilaw    = utf8_decode($_POST['acrescimo_valor_komilaw']);

                  $query = "INSERT INTO acrescimos (acrescimo_nome, acrescimo_valor_normal, acrescimo_valor_komilaw )  VALUES ('$acrescimo_nome','$acrescimo_valor_normal','$acrescimo_valor_komilaw')";
                  $rst = mysql_query($query) or die(mysql_error());
                  echo "<script>window.location = 'listar-cardapio-acrescimos.php';</script>";
                }
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Nome do acrescimo</label>
                                            <input class="form-control" name="acrescimo_nome" id="acrescimo_nome">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor do acrescimo NORMAL</label>
                                            <input class="form-control valor" name="acrescimo_valor_normal" id="acrescimo_valor_normal">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor do acrescimo KOMILAW</label>
                                            <input class="form-control valor" name="acrescimo_valor_komilaw" id="acrescimo_valor_komilaw">
                                        </div>

                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
</body>

</html>
