<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">VALORES DE FRETES</h1>
                </div>
            </div>
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    if (!empty($_POST['acrescimo_nome']) && !empty($_POST['acrescimo_valor_normal']) && !empty($_POST['acrescimo_valor_komilaw'])) {
                        $acrescimo_id = $_POST['acrescimo_id'];
                        $acrescimo_nome = $_POST['acrescimo_nome'];
                        $acrescimo_valor_normal = $_POST['acrescimo_valor_normal'];
                        $acrescimo_valor_komilaw = $_POST['acrescimo_valor_komilaw'];
                        $sql = "UPDATE acrescimos SET
                            acrescimo_nome = '$acrescimo_nome',
                            acrescimo_valor_normal = $acrescimo_valor_normal,
                            acrescimo_valor_komilaw = $acrescimo_valor_komilaw
                            where acrescimo_id = $acrescimo_id
                            ";
                        $sql = mysql_query("$sql");
                        
                        echo "<meta http-equiv='refresh' content='3;URL=listar-cardapio-acrescimos.php' /> Acréscimo atualizado com sucesso, aguarde.";
                    }
                }


            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <?php
                                      $id = $_GET['id'];
                                      $consulta = "SELECT * FROM acrescimos WHERE acrescimo_id = $id";
                                      $rst = mysql_query($consulta);
                                      $linha = mysql_fetch_array($rst);
                                        $acrescimo_id  = utf8_encode(nl2br($linha['acrescimo_id']));
                                        $acrescimo_nome  = utf8_encode(nl2br($linha['acrescimo_nome']));
                                        $acrescimo_preco1  = $linha['acrescimo_valor_normal'];
                                        $acrescimo_preco2  = $linha['acrescimo_valor_komilaw'];
                                    ?>

                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Nome do acrescimo</label>
                                            <input class="form-control" value="<?=$acrescimo_nome?>" name="acrescimo_nome" id="acrescimo_nome">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor do acrescimo NORMAL</label>
                                            <input class="form-control valor" value="<?=$acrescimo_preco1?>" name="acrescimo_valor_normal" id="acrescimo_valor_normal">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor do acrescimo KOMILAW</label>
                                            <input class="form-control valor" value="<?=$acrescimo_preco2?>" name="acrescimo_valor_komilaw" id="acrescimo_valor_komilaw">
                                        </div>

                                        <input type="hidden" value="<?=$acrescimo_id?>" name="acrescimo_id"> 

                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"?>

</body>

</html>
