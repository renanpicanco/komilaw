<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">LISTAR ACRÉSCIMOS DO CARDÁPIO</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID do acréscimo</th>
                                        <th>Nome do Acréscimo</th>
                                        <th>Valor Normal</th>
                                        <th>Valor Komilaw</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                      $consulta = "SELECT * FROM acrescimos order by acrescimo_nome ASC";
                                      $rst = mysql_query($consulta);
                                      while($linha = mysql_fetch_array($rst)) {
                                        $acrescimo_id  = utf8_encode(nl2br($linha['acrescimo_id']));
                                        $acrescimo_nome  = utf8_encode(nl2br($linha['acrescimo_nome']));
                                        $acrescimo_preco_1  = utf8_encode(nl2br($linha['acrescimo_valor_normal']));
                                        $acrescimo_preco_2  = utf8_encode(nl2br($linha['acrescimo_valor_komilaw']));
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?=$acrescimo_id?></td>
                                        <td class="center"><?=$acrescimo_nome?></td>
                                        <td class="center"><?=$acrescimo_preco_1?></td>
                                        <td class="center"><?=$acrescimo_preco_2?></td>
                                        <td class="center">
                                          <a href="cardapio-acrescimos-editar.php?id=<?=$acrescimo_id?>">Editar</a>
                                          <span> - </span>
                                          <a href="cardapio-acrescimos-excluir.php?id=<?=$acrescimo_id?>">Excluir</a>
                                        </td>
                                    </tr>
                                  <? } ?>
                                </tbody>
                            </table>
                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>

    <?php include "footer.php"?>
</body>

</html>
