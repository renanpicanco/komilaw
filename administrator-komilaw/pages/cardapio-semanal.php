<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">CARDÁPIO SEMANAL</h1>
                </div>
            </div>
            <?php   
                $id = strip_tags($_GET['cardapio_semanal_id']);
                if (isset($_POST['cardapio_semanal_dia']) && isset($_POST['cardapio_semanal_data']) && isset($_POST['cardapio_semanal_cardapio'])) {
                    $cardapio_semanal_dia           = utf8_decode($_POST['cardapio_semanal_dia']);
                    $cardapio_semanal_data          = utf8_decode($_POST['cardapio_semanal_data']);
                    $cardapio_semanal_cardapio      = utf8_decode($_POST['cardapio_semanal_cardapio']);

                    $sql = "UPDATE cardapio_semanal SET
                    cardapio_semanal_dia = '$cardapio_semanal_dia',
                    cardapio_semanal_data = '$cardapio_semanal_data',
                    cardapio_semanal_cardapio = '$cardapio_semanal_cardapio'
                    where cardapio_semanal_id = '$id'";
                    $sql = mysql_query("$sql");
                    echo "<meta http-equiv='refresh' content='3;URL=cardapio-semanal.php?cardapio_semanal_id=". $id ."' />
                    Enviado com sucesso, aguarde!";
                }


                $consulta = "SELECT * FROM cardapio_semanal WHERE cardapio_semanal_id = '$id'";
                $rst = mysql_query($consulta);
                $linha = mysql_fetch_array($rst);
                    $cardapio_semanal_dia           = utf8_encode($linha['cardapio_semanal_dia']);
                    $cardapio_semanal_data          = utf8_encode($linha['cardapio_semanal_data']);
                    $cardapio_semanal_cardapio      = utf8_encode($linha['cardapio_semanal_cardapio']);
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Dia da semana</label>
                                            <input class="form-control" name="cardapio_semanal_dia" value="<?php echo $cardapio_semanal_dia; ?>">
                                            <p class="help-block">Exemplo: Segunda-feira</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Data da semana</label>
                                            <input class="form-control" name="cardapio_semanal_data" value="<?php echo $cardapio_semanal_data; ?>">
                                            <p class="help-block">Exemplo: 14 de maio, 2018</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Digite aqui seu cardápio:</label>
                                            <textarea class="form-control" rows="20" name="cardapio_semanal_cardapio"><?php echo $cardapio_semanal_cardapio; ?>
                                            </textarea>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"?>
</body>

</html>

</body>

</html>
