<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">CARDÁPIO PASTELARIA</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID - Nome do produto</th>
                                        <th>Preço Normal</th>
                                        <th>Preço Komilaw</th>
                                        <th>Preço Aperitivo</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                      $consulta = "SELECT * FROM cardapio_pastelaria order by cat_id ASC";
                                      $rst = mysql_query($consulta);
                                      while($linha = mysql_fetch_array($rst)) {
                                        $produto_id  = utf8_encode(nl2br($linha['id']));
                                        $produto_nome  = utf8_encode(nl2br($linha['nome']));
                                        $produto_preco_1  = utf8_encode(nl2br($linha['preco_normal']));
                                        $produto_preco_2  = utf8_encode(nl2br($linha['preco_komilaw']));
                                        $produto_preco_3  = utf8_encode(nl2br($linha['preco_aperitivo']));
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?=$produto_id." - ".$produto_nome?></td>
                                        <td class="center"><?=$produto_preco_1?></td>
                                        <td class="center"><?=$produto_preco_2?></td>
                                        <td class="center"><?=$produto_preco_3?></td>
                                        <td class="center">
                                          <a href="cardapio-pastelaria.php?cardapio_id=<?=$produto_id?>">Editar</a>
                                          <span> - </span>
                                          <a href="cardapio-pastelaria-excluir.php?id=<?=$produto_id?>">Excluir</a>
                                        </td>
                                    </tr>
                                  <? } ?>
                                </tbody>
                            </table>
                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>

    <?php include "footer.php"?>
</body>

</html>
