<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">CARDÁPIO PASTELARIA</h1>
                </div>
            </div>
            <?php
                $produtoId = null;

                if ($_SERVER['REQUEST_METHOD'] == "GET") {
                    if (!empty($_GET['cardapio_id'])) {
                        $produtoId = $_GET['cardapio_id'];

                        /** Busca item do cardapio pelo id do parametro cardapio_id */

                        $sqlCardapioItem = "SELECT * FROM cardapio_pastelaria WHERE id = $produtoId";
                        $cardapioResult  = mysql_query($sqlCardapioItem);
                        $cardapioItem    = mysql_fetch_array($cardapioResult);

                        /** Busca ascrescimos do cardapio item */

                        $sqlAcrescimos    = "SELECT * FROM con_acrescimo_produtos WHERE con_produto_id = $produtoId";
                        $acrescimosResult = mysql_query($sqlAcrescimos);

                        $aAItens = [];
                        while ($aRow = mysql_fetch_array($acrescimosResult)) {
                            $aAItens[] = $aRow['con_acrescimo_id'];
                        }
                    }
                }

                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    if (!empty($_POST['id'])) $produtoId = $_POST['id'];

                    if (!empty($_POST['nome']) && !empty($_POST['descricao'])) {
                        $cat_id             = utf8_decode($_POST['cat_id']);
                        $nome               = utf8_decode($_POST['nome']);
                        $descricao          = utf8_decode($_POST['descricao']);
                        $preco_normal       = utf8_decode($_POST['preco_normal']);
                        $preco_komilaw      = (empty($_POST['preco_komilaw']) || $_POST['preco_komilaw'] == '0.00')? '' : $_POST['preco_komilaw'];
                        $preco_aperitivo    = (empty($_POST['preco_aperitivo']) || $_POST['preco_aperitivo'] == '0.00')? '' : $_POST['preco_aperitivo'];

                      if (!empty($produtoId)) {
                        $query = "
                            UPDATE cardapio_pastelaria
                               SET
                                    cat_id = '$cat_id',
                                    nome = '$nome',
                                    descricao = '$descricao',
                                    preco_normal = '$preco_normal',
                                    preco_komilaw = '$preco_komilaw',
                                    preco_aperitivo = '$preco_aperitivo'
                             WHERE id = $produtoId;
                        ";
                      } else {
                        $query = "INSERT INTO cardapio_pastelaria (cat_id, nome, descricao, preco_normal, preco_komilaw, preco_aperitivo )  VALUES ('$cat_id', '$nome','$descricao','$preco_normal','$preco_komilaw','$preco_aperitivo')";
                      }

                      $rst = mysql_query($query) or die(mysql_error());

                      if (empty($produtoId))
                        $produtoId = mysql_insert_id();

                      $delete = "DELETE FROM con_acrescimo_produtos where con_produto_id = $produtoId";
                      mysql_query($delete);                   

                      if (!empty($produtoId) && !empty($_POST['acrescimos'])) {
                        foreach ($_POST['acrescimos'] as $keyA => $a) {
                            try {
                                $queryA = "INSERT INTO con_acrescimo_produtos (con_acrescimo_id, con_produto_id) VALUES ($a, $produtoId)";
                                $rsta   = mysql_query($queryA);
                            
                            } catch (Exception $e) {
                                die('Não foi possível salvar o acrescimo.'); 
                            }                          
                        }  
                      }

                      echo "<meta http-equiv='refresh' content='3;URL=listar-cardapio-pastelaria.php' /> Salvo com sucesso, aguarde para ir para a lista de produtos";
                    } else {
                        echo "Não inserido, tente novamente confira os dados!";
                    }
                }

                $consulta = "SELECT * FROM categoria_produtos order by id ASC";
                $rst = mysql_query($consulta);
            ?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST">
                                        <input type="hidden" name="id" value="<?php echo @$cardapioItem['id'] ?>">
                                        <div class="form-group">
                                            <label>Selecione uma categoria</label>
                                            <select class="form-control" name="cat_id" id="cat_id">
                                            <?php
                                                while($linha = mysql_fetch_array($rst)) {
                                                    $categoria_produtos_id  = utf8_encode(nl2br($linha['id']));
                                                    $categoria_produtos_nome  = utf8_encode(nl2br($linha['nome']));
                                            ?>
                                                <option value="<?php echo $categoria_produtos_id; ?>" <?php echo (@$cardapioItem['cat_id'] == $categoria_produtos_id ? 'selected' : '') ?>>
                                                    <?php echo $categoria_produtos_nome; ?>        
                                                </option>
                                            <? } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nome do produto</label>
                                            <input class="form-control" name="nome" id="nome" value="<?php echo @$cardapioItem['nome'] ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Ingredientes:</label>
                                            <textarea class="form-control" rows="5" name="descricao" id="descricao"><?php echo @$cardapioItem['descricao'] ?></textarea>
                                        </div>

                                        <label>Preço Normal</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control valor" name="preco_normal" id="preco_normal" value="<?php echo @$cardapioItem['preco_normal'] ?>">
                                        </div>

                                        <label>Preço Komilaw</label>
                                        <div class="form-group input-group">  
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" name="preco_komilaw" id="preco_komilaw" value="<?php echo @$cardapioItem['preco_komilaw'] ?>">
                                        </div

                                        <label>Preço Aperitivo</label>
                                        <div class="form-group input-group"> 
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control valor" name="preco_aperitivo" id="preco_aperitivo" value="<?php echo @$cardapioItem['preco_aperitivo'] ?>">
                                        </div>

                                        <h4>SELECIONE OS ACRÉSCIMOS PARA ESTA OPÇÃO</h4>

                                        <div class="form-group">
                                            <?php
                                              $consulta = "SELECT * FROM acrescimos";
                                              $rst = mysql_query($consulta);
                                              while($linha = mysql_fetch_array($rst)) {
                                                $acrescimo_id  = utf8_encode(nl2br($linha['acrescimo_id']));
                                                $acrescimo_nome  = utf8_encode(nl2br($linha['acrescimo_nome']));
                                                $acrescimo_preco_1  = utf8_encode(nl2br($linha['acrescimo_valor_normal']));
                                                $acrescimo_preco_2  = utf8_encode(nl2br($linha['acrescimo_valor_komilaw']));

                                                $cheked = '';
                                                if (!empty($aAItens) && in_array($acrescimo_id, $aAItens))
                                                    $cheked = 'checked=cheked';
                                            ?>
                                            <div class="checkbox">
                                                <label for="<?php echo $acrescimo_nome ?>">
                                                    <input type="checkbox" id="<?php echo $acrescimo_nome ?>" name="acrescimos[]" value="<?php echo $acrescimo_id?>" <?php echo $cheked ?>><?php echo $acrescimo_nome ?>
                                                </label>
                                            </div>
                                            <? } ?>
                                        </div>
                                      
                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"?>
</body>

</html>
