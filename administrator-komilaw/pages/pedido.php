<?php include 'head.php'?>
    <style>
        td {
            margin-bottom: 10px;
        }
        .imprimir {
            width: 100%;
            text-align: center;
            display: inline-block;
        }

        .imprimir a {
            padding:5px 10px;
            background: #444;
            color: white;
            display: inline-block;
            margin:10px;
        }
        @media print {
          body * {
            visibility: hidden;
          }
          #printable, #printable * {
            visibility: visible;
          }
          #printable {
            position: fixed;
            left: 0;
            top: 0;
          }
        }
    </style>
    <div  id="printable">

    <table style="margin:0 auto; width: 100%; max-width: 500px;">
    <?php
      $id = $_GET['id'];
      $consulta = "SELECT * FROM pedidos WHERE pedido_id = $id";
      $rst = mysql_query($consulta);
      $linha = mysql_fetch_array($rst);
        $pedido_id  = utf8_encode(nl2br($linha['pedido_id']));
        $pedido_nome  = utf8_encode(nl2br($linha['pedido_nome']));
        $pedido_data  = utf8_encode(nl2br($linha['pedido_data']));
        $pedido_bairro  = utf8_encode(nl2br($linha['pedido_bairro']));
        $pedido_endereco  = utf8_encode(nl2br($linha['pedido_endereco']));
        $pedido_valor  = utf8_encode(nl2br($linha['pedido_valor_total']));
        $pedido_status  = utf8_encode(nl2br($linha['pedido_status']));
        $pedido_pedido  = utf8_encode(nl2br($linha['pedido_pedido']));
    ?>
    <tr class="odd gradeX">
        <td class="center"><strong>Número do pedido:</strong> <?=$pedido_id?></td>
    </tr>
    <tr class="odd gradeX">
        <td class="center"><strong>Data: </strong><?=$pedido_data?></td>
    </tr>
    <tr class="odd gradeX">
        <td class="center"><strong>Cliente: </strong><?=$pedido_nome?></td>
    </tr>
    <tr class="odd gradeX">
        <td class="center"><strong>Bairro da entrega:</strong> <?=$pedido_bairro?></td>
    </tr>
     <tr class="odd gradeX">
        <td class="center"><strong>Endereço:</strong> <?=$pedido_endereco?></td>
    </tr>
    <tr class="odd gradeX"><td class="center"><strong><br>PEDIDO:</strong><br></td></tr>
    <tr class="odd gradeX">
        <td class="center"><?=$pedido_pedido?></td>
    </tr>
    <tr class="odd gradeX"><td><h4>VALOR TOTAL: <?=$pedido_valor?></h4></td></tr>
</table>
</div>
<div class="imprimir">
    <a onclick='printDiv();'>Imprimir pedido</a>
    <a href="check-status.php?id=<?=$pedido_id?>">Finalizar pedido</a>
</div>

<script>
    function printDiv() 
{

  var divToPrint=document.getElementById('printable');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>