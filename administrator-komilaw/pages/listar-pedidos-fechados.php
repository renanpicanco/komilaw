<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">LISTAGEM DE PEDIDOS</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID do pedido</th>
                                        <th>Nome do cliente</th>
                                        <th>Data</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                      $consulta = "SELECT * FROM pedidos WHERE pedido_status = 1 order by pedido_id DESC";
                                      $rst = mysql_query($consulta);
                                      while($linha = mysql_fetch_array($rst)) {
                                        $pedido_id  = utf8_encode(nl2br($linha['pedido_id']));
                                        $pedido_nome  = utf8_encode(nl2br($linha['pedido_nome']));
                                        $pedido_bairro  = utf8_encode(nl2br($linha['pedido_bairro']));
                                        $pedido_data  = utf8_encode(nl2br($linha['pedido_data']));
                                        $pedido_valor  = utf8_encode(nl2br($linha['pedido_valor_total']));
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?=$pedido_id?></td>
                                        <td class="center"><?=$pedido_nome?></td>
                                        <td class="center"><?=$pedido_data?></td>
                                        <td class="center"><?=$pedido_bairro?></td>
                                        <td class="center">R$ <?=$pedido_valor?></td>
                                        <td class="center">
                                          <a href="pedido.php?id=<?=$pedido_id?>" target="_Blank">Visualizar</a>
                                        </td>
                                    </tr>
                                  <? } ?>
                                </tbody>
                            </table>
                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>

    <?php include "footer.php"?>
</body>

</html>
