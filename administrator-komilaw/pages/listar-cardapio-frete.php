<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">LISTAR BAIRROS DE FRETE</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID do Bairro</th>
                                        <th>Nome do Bairro</th>
                                        <th>Valor da tele</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                      $consulta = "SELECT * FROM cardapio_frete order by cardapio_frete_bairro ASC";
                                      $rst = mysql_query($consulta);
                                      while($linha = mysql_fetch_array($rst)) {
                                        $frete_id  = utf8_encode(nl2br($linha['cardapio_frete_id']));
                                        $frete_nome  = utf8_encode(nl2br($linha['cardapio_frete_bairro']));
                                        $frete_preco  = utf8_encode(nl2br($linha['cardapio_frete_valor']));
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?=$frete_id?></td>
                                        <td class="center"><?=$frete_nome?></td>
                                        <td class="center"><?=$frete_preco?></td>
                                        <td class="center">
                                          <a href="cardapio-frete-editar.php?id=<?=$frete_id?>">Editar</a>
                                          <span> - </span>
                                          <a href="cardapio-frete-excluir.php?id=<?=$frete_id?>">Excluir</a>
                                        </td>
                                    </tr>
                                  <? } ?>
                                </tbody>
                            </table>
                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>

    <?php include "footer.php"?>
</body>

</html>
