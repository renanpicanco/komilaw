<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Pedidos Pastelaria<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="listar-pedidos-abertos.php">Listar Pedidos aberto</a>
                        <a href="listar-pedidos-fechados.php">Listar Pedidos fechados</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Cardápio semanal<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=0">Segunda - Feira</a>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=1">Terça - Feira</a>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=2">Quarta - Feira</a>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=3">Quinta - Feira</a>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=4">Sexta - Feira</a>
                        <a href="cardapio-semanal.php?cardapio_semanal_id=5">Sábado</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Cardápio pastelaria<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="listar-cardapio-pastelaria.php">Listar produto</a>
                        <a href="cardapio-pastelaria.php">Incluir produto</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Acréscimos<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="listar-cardapio-acrescimos.php">Listar acréscimos</a>
                        <a href="cardapio-acrescimos.php">Incluir acréscimo</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Valor de entregas<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="listar-cardapio-frete.php">Listar bairros</a>
                        <a href="cardapio-frete.php">Incluir bairros</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
</div>
