<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">CARDÁPIO PASTELARIA</h1>
                </div>
            </div>
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    if (!empty($_POST['nome']) && !empty($_POST['descricao'])) {
                        $id                 = $_POST['id'];
                        $cat_id             = utf8_decode($_POST['cat_id']);
                        $nome               = utf8_decode($_POST['nome']);
                        $descricao          = utf8_decode($_POST['descricao']);
                        $preco_normal       = utf8_decode($_POST['preco_normal']);
                        $preco_komilaw      = (empty($_POST['preco_komilaw']) || $_POST['preco_komilaw'] == '0.00')? '' : $_POST['preco_komilaw'];
                        $preco_aperitivo    = (empty($_POST['preco_aperitivo']) || $_POST['preco_aperitivo'] == '0.00')? '' : $_POST['preco_aperitivo'];

                      $query = "INSERT INTO cardapio_pastelaria (cat_id, id, nome, descricao, preco_normal, preco_komilaw, preco_aperitivo )  VALUES ('$cat_id', '$id','$nome','$descricao','$preco_normal','$preco_komilaw','$preco_aperitivo')";
                      $rst = mysql_query($query) or die(mysql_error());
                      echo "<meta http-equiv='refresh' content='3;URL=listar-cardapio-pastelaria.php' /> Inserido com sucesso, aguarde para ir para a lista de produtos";
                    } else {
                        echo "Não inserido, tente novamente confira os dados!";
                    }
                }

                $consulta = "SELECT * FROM categoria_produtos order by id ASC";
                $rst = mysql_query($consulta);
            ?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Selecione uma categoria</label>
                                            <select class="form-control" name="cat_id" id="cat_id">
                                            <?php
                                                while($linha = mysql_fetch_array($rst)) {
                                                    $categoria_produtos_id  = utf8_encode(nl2br($linha['id']));
                                                    $categoria_produtos_nome  = utf8_encode(nl2br($linha['nome']));
                                            ?>
                                                <option value="<?php echo $categoria_produtos_id; ?>">
                                                    <?php echo $categoria_produtos_nome; ?>        
                                                </option>
                                            <? } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nome do produto</label>
                                            <input class="form-control" name="nome" id="nome">
                                        </div>
                                        <div class="form-group">
                                            <label>Ingredientes:</label>
                                            <textarea class="form-control" rows="5" name="descricao" id="descricao"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Preço Normal</label>
                                            <input class="form-control valor" name="preco_normal" id="preco_normal">
                                        </div>

                                        <div class="form-group">
                                            <label>Preço Komilaw</label>
                                            <input class="form-control valor" name="preco_komilaw" id="preco_komilaw">
                                        </div>

                                        <div class="form-group">
                                            <label>Preço Aperitivo</label>
                                            <input class="form-control valor" name="preco_aperitivo" id="preco_aperitivo">
                                        </div>

                                        <h4>SELECIONE OS ACRÉSCIMOS PARA ESTA OPÇÃO</h4>
                                        <?/* AQUI VAI OS CHECKBOX DE ACRESCIMOS */?>
                                        
                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "footer.php"?>
</body>

</html>
