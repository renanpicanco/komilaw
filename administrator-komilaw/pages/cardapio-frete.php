<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">VALORES DE FRETES</h1>
                </div>
            </div>
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    if (!empty($_POST['cardapio_frete_bairro']) && !empty($_POST['cardapio_frete_valor'])) {
                        $cardapio_frete_bairro    = utf8_decode($_POST['cardapio_frete_bairro']);
                        $cardapio_frete_valor     = utf8_decode($_POST['cardapio_frete_valor']);

                        $query = "INSERT INTO cardapio_frete (cardapio_frete_bairro, cardapio_frete_valor)  VALUES ('$cardapio_frete_bairro','$cardapio_frete_valor')";
                        $rst = mysql_query($query) or die(mysql_error());
                        echo "<meta http-equiv='refresh' content='3;URL=listar-cardapio-frete.php' /> Inserido com sucesso, aguarde para ir para a listagem";
                    } else {
                        echo "Não inserido, tente novamente confira os dados!";
                    }
                }
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Nome do bairro</label>
                                            <input class="form-control" name="cardapio_frete_bairro" id="cardapio_frete_bairro">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor da entrega</label>
                                            <input class="form-control valor" name="cardapio_frete_valor" id="cardapio_frete_valor">
                                        </div>
                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"?>

</body>

</html>
