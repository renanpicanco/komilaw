<?php include 'head.php'?>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <?php include "nav.php" ?> 

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">VALORES DE FRETES</h1>
                </div>
            </div>
            <?php
                if($_SERVER['REQUEST_METHOD'] == "POST"){
                    if (!empty($_POST['frete_id']) && !empty($_POST['cardapio_frete_bairro']) && !empty($_POST['cardapio_frete_valor'])) {
                        $cardapio_frete_id = $_POST['frete_id'];
                        $cardapio_frete_bairro = $_POST['cardapio_frete_bairro'];
                        $cardapio_frete_valor = $_POST['cardapio_frete_valor'];
                        $sql = "UPDATE cardapio_frete SET
                            cardapio_frete_bairro = '$cardapio_frete_bairro',
                            cardapio_frete_valor = '$cardapio_frete_valor'
                            where cardapio_frete_id = $cardapio_frete_id
                            ";
                        $sql = mysql_query("$sql");
                        
                        echo "<meta http-equiv='refresh' content='3;URL=listar-cardapio-frete.php' /> Frete atualizado com sucesso, aguarde.";
                    }
                }


            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alterando os dados automaticamente serão atualizados em seu site, <strong> Não esqueça de sempre seguir o padrão de exemplo</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <?php
                                      $id = $_GET['id'];
                                      $consulta = "SELECT * FROM cardapio_frete WHERE cardapio_frete_id = $id";
                                      $rst = mysql_query($consulta);
                                      $linha = mysql_fetch_array($rst);
                                        $frete_id  = utf8_encode(nl2br($linha['cardapio_frete_id']));
                                        $frete_nome  = utf8_encode(nl2br($linha['cardapio_frete_bairro']));
                                        $frete_preco  = utf8_encode(nl2br($linha['cardapio_frete_valor']));
                                    ?>

                                    <form role="form" method="POST">
                                        <div class="form-group">
                                            <label>Nome do bairro</label>
                                            <input class="form-control" name="cardapio_frete_bairro" id="cardapio_frete_bairro" value="<?=$frete_nome?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Valor da entrega</label>
                                            <input class="form-control valor" name="cardapio_frete_valor" id="cardapio_frete_valor" value=<?=$frete_preco?>>
                                        </div>
                                        <input type="hidden" value="<?=$frete_id?>" name="frete_id">
                                        <button type="submit" class="btn btn-default">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"?>

</body>

</html>
