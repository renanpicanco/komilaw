
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Komilaw, Buffet e Pastelaria</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="A melhor pastelaria de Pelotas"/>
	<meta name="keywords" content="melhor pastel de pelotas, pastel, pelotas, rs, pastelaria em pelotas, pastelaria pelotas, pasteis, buffet pelotas, melhor buffet em pelotas, viandas pelotas, marmitas pelotas, ala minuta pelotas, qualidade, webhnet" />
	<meta name="author" content="webhnet - oficina web" />


  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="css/simple-line-icons.css">
	<!-- Datetimepicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<div id="fh5co-container">
		<div id="fh5co-home" class="js-fullheight" data-section="home">

			<a href="#">
				
			</a>

			<div class="flexslider">
				
				<div class="fh5co-overlay"></div>
				<div class="fh5co-text">
					<div class="container">
						<div class="row">
							<h1 class="to-animate">
								<img src="images/logo.png" class="logo">
							</h1>
							<nav class="midias-sociais">
								<a href="https://www.facebook.com/Komilawpasteis/"><i class="icon-facebook"></i></a>
								<a href="jhttps://www.instagram.com/explore/locations/309500021/komilaw-buffet-e-pastelaria/" target="_blank"><i class="icon-instagram"></i></a>
							</nav>
							<h2 class="to-animate">Role para baixo e delicie-se.</h2>
						</div>
					</div>
				</div>
			  	<ul class="slides to-animate" >
			   	<li style="background-image: url(images/slide_1.jpg);" data-stellar-background-ratio="0.5"></li>
			   	<li style="background-image: url(images/slide_2.jpg);" data-stellar-background-ratio="0.5"></li>
			   	<li style="background-image: url(images/slide_3.jpg);" data-stellar-background-ratio="0.5"></li>
			  	</ul>

			</div>
			
		</div>
		
		<div class="js-sticky">
			<div class="fh5co-main-nav">
				<div class="container-fluid">
					<div class="fh5co-menu-1">
						<a href="#" data-nav-section="home">Home</a>
						<a href="#" data-nav-section="about">Sobre nós</a>
						<a href="#" data-nav-section="features">Nossas delícias</a>
					</div>
					<div class="fh5co-logo">
						<a href="index.html">
							<img src="images/logo.png" class="logo-menu">
						</a>
					</div>
					<div class="fh5co-menu-2">
						<a href="#" data-nav-section="events">Buffet</a>
						<a href="#" data-nav-section="menu">Estrutura</a>
						<a href="#" data-nav-section="reservation">Contato</a>
					</div>
				</div>
				
			</div>
		</div>

		<div id="fh5co-about" data-section="about">
			<div class="fh5co-2col fh5co-bg to-animate-2" style="background-image: url(images/res_img_1.jpg)"></div>
			<div class="fh5co-2col fh5co-text">
				<h2 class="heading to-animate">Sobre nós</h2>
				<p class="to-animate">
					Em  Agosto de 1.996 à realização de um sonho concretizou-se. Nascia uma Empresa no ramo da alimentação Fora do Lar  genuínamente familiar.<br/><br/>

					Berenice Ferreira da Fonseca, juntamente com Margarete Fonseca Dutra e seu esposo, Luiz Dutra com mais  dois cunhados começaram à trabalhar atendendo de Segunda à Segunda ao meio dia com BUFFET POR KILO , recomeçando à tardinha com LANCHES e À LA CARTE até à meia noite , situação que perdurou por oito meses seguidos, sendo então que a partir daquela data resolveram dedicar-se exclusivamente ao BUFFET  ao meio dia. Com um espaço pequeno, 80 m quadrados, tinham acomodações para 25 pessoas e na época já serviam em torno de 120/130 refeições.<br/><br/>

					Com muita dedicação, trabalho, afinco, amor pela profissão e principalmente com a experiência de estarem acostumados à serem servidos em sua residência familiar 20/30 refeições diariamente que poderiam facilmente chegar  a 90/100 em suas comemorações intimas de aniversário, natal ou fim de ano, especializaram-se numa culinária  típica brasileira, caseira  com ênfase na culinária portuguesa, suas origens e espanhola, origem de seu esposo partindo para outra especialização  a italiana .<br/>
					Atualmente o RESTAURANTE serve de 350/450 refeições diárias  num espaço amplo e agradável, climatizado e com estacionamento próprio numa área total de 1.050 m quadrados, com variado buffet de pratos quentes, grelhados e saladas variadas.<br/><br/>

					Contamos hoje com uma equipe de 18 funcionários qualificados e treinados tanto para manipulação do alimento como para atendimento ao cliente.<br/><br/>

					Em novembro de 2008 foi inaugurada a Pastelaria do komilaw em anexo ao restaurante servindo + de 30 sabores de pastéis com a massa caseira do komilaw. O sucesso já está garantido e mostrou que veio para ficar pois a qualidade do produto oferecido é o grande diferencial de todo o nosso trabalho.  Atualmente é gerenciado pelo filho do casal o sr, Fabrício Fonseca Dutra.<br/><br/>

					Em janeiro de 2010  um grande passo foi dado no sentido de qualificação e garantia ao cliente  do consumo do alimento seguro, ingressamos  no PAS (PROGRAMA DO ALIMENTO SEGURO) do Governo Federal ao qual o estabelecimento tem que adequar-se a todas as normas da ANVISA, órgão que regulamenta todos os procedimentos de manipulação com alimentos.<br/><br/>
					 
					<strong style="color:white; font-size: 16px; margin-bottom: 15px;">KOMIL’AW</strong><br/>
					 
					<span style="color:white;">MISSÃO:</span> PROFISSIONALISMO COM EXCELÊNCIA NA PRODUÇÃO DO ALIMENTO SEGURO E QUALIDADE NO ATENDIMENTO AO CLIENTE.<br/>
					 
					<span style="color:white;">VISÃO:</span> CRESCIMENTO DA EMPRESA COM METODOLOGIA, ESTRATÉGIA E PLANEJAMENTO.<br/>
					 
					<span style="color:white;">VALOR:</span> VALORIZAÇÃO COM COMPROMETIMENTO E VALORIZAÇÃO DO FUNCIONÁRIO.</p>
			</div>
		</div>

		<div id="fh5co-type" style="background-image: url(images/slide_3.jpg);" data-stellar-background-ratio="0.5">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-3 to-animate">
						<div class="fh5co-type">
							<h3 class="with-icon icon-1">Frutas</h3>
							<p>Selecionamos as melhores frutas, com qualidade para você.</p>
						</div>
					</div>
					<div class="col-md-3 to-animate">
						<div class="fh5co-type">
							<h3 class="with-icon icon-2">Frutos do mar</h3>
							<p>Já é cultura em nosso buffet o melhor que nossas águas nos oferece. Toda semana tem frutos do mar.</p>
						</div>
					</div>
					<div class="col-md-3 to-animate">
						<div class="fh5co-type">
							<h3 class="with-icon icon-3">Saladas</h3>
							<p>Prezando por sua qualidade de vida, sabemos que um almoço não é o mesmo se não tiver aquele buffezão de saladas dos mais variados tipos.</p>
						</div>
					</div>
					<div class="col-md-3 to-animate">
						<div class="fh5co-type">
							<h3 class="with-icon icon-4">Churrasco</h3>
							<p>Somos uma empresa genuinamente gaúcha, é claro que sempre vai existir o dia do churrasco, confira abaixo nosso cardápio do buffet.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		

		<div id="fh5co-featured" data-section="features">
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading to-animate">Nossas delícias</h2>
						<p class="sub-heading to-animate">Trabalhamos com Buffet, Pastéis, Delivery de almoços e outras maravilhas.</p>
					</div>
				</div>
				<div class="row">
					<div class="fh5co-grid">
						<div class="fh5co-v-half to-animate-2">
							<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_9.jpg)"></div>
							<div class="fh5co-v-col-2 fh5co-text fh5co-special-1 arrow-left">
								<h2>Alaminuta</h2>
								<span class="pricing"></span>
								<p>Não sabe ainda o que almoçar? Liga e peça uma de nossas a la minutas.</p>
							</div>
						</div>
						<div class="fh5co-v-half">
							<div class="fh5co-h-row-2 to-animate-2">
								<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_2.jpg)"></div>
								<div class="fh5co-v-col-2 fh5co-text arrow-left">
									<h2>Pastéis</h2>
									<span class="pricing"></span>
									<p>Olha essa delícia de goiabada com queijo. Esse é apenas um de uma variedade incrível de sabores</p>
								</div>
							</div>
							<div class="fh5co-h-row-2 fh5co-reversed to-animate-2">
								<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_8.jpg)"></div>
								<div class="fh5co-v-col-2 fh5co-text arrow-right">
									<h2>Pastel aberto</h2>
									<span class="pricing"></span>
									<p>Ainda não conhece? Não? Liga pra cá peça o seu agora.</p>
								</div>
							</div>
						</div>

						<div class="fh5co-v-half">
							<div class="fh5co-h-row-2 fh5co-reversed to-animate-2">
								<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_7.jpg)"></div>
								<div class="fh5co-v-col-2 fh5co-text arrow-right">
									<h2>Porções</h2>
									<span class="pricing"></span>
									<p>Em nossa Pastelaria você encontra uma variada linha de porções.</p>
								</div>
							</div>
							<div class="fh5co-h-row-2 to-animate-2">
								<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_6.jpg)"></div>
								<div class="fh5co-v-col-2 fh5co-text arrow-left">
									<h2>Buffet de saladas</h2>
									<span class="pricing"></span>
									<p>Prezamos também pela sua saúde, com isso temos uma grande variedade de saladas em nosso buffet.</p>
								</div>
							</div>
						</div>
						<div class="fh5co-v-half to-animate-2">
							<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(images/res_img_3.jpg)"></div>
							<div class="fh5co-v-col-2 fh5co-text fh5co-special-1 arrow-left">
								<h2>Vem almoçar com a gente</h2>
								<span class="pricing"></span>
								<p>Nosso buffet consiste em uma gran de diversidade culinária, alimentos da maior qualidade de preparo. Nossa empresa possui selo categoria A fornecido pela ANVISA.</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

		

		<div id="fh5co-events" data-section="events" style="background-image: url(images/slide_2.jpg);" data-stellar-background-ratio="0.5">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2 to-animate">
						<h2 class="heading">Buffet</h2>
						<p class="sub-heading">Confira aqui nosso cardápio atualizado com tudo que vai ter em nosso Buffet durante a semana.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Segunda-feira</h3>
							<span class="fh5co-event-meta">14 de maio, 2018</span>
							<p>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								RISOTO DE FRANGO<br/>
								FEIJÃO S/CARNES<br/>
								TORTEI AO MOLHO VERMELHO<br/>
								FETTUCCINE FRESCO CASEIRO<br/>
								MASSA 4 QUEIJOS DE LEGUMES<br/>
								BATATA FRITA<br/>
								ABOBRINHA ITALIANA<br/>
								MOLHO DE TOMATES<br/>
								QUISADINHO DE MILHO VERDE<br/>
								CROQUETINHO DE CARNE<br/>
								PASTEL DE FRANGO<br/>
								BATATAS PORTUGUÊSA<br/>
								TATU(LAGARTO) AO MOLHO<br/>
								BIFES DE RÊS<br/>
								FILÉ PEITO FRANGO GRELHADO<br/>
								FILÉ DE PEIXE GRELHADO<br/>
								ISCAS DE PEIXE À DORÊ<br/>
								FRANGO <br/>
								SALSICHÃO<br/>
								RISOLE DE QUEIJO<br/>
								BUFFET DE SALADAS & DOCES
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Terça-feira</h3>
							<span class="fh5co-event-meta">15 de maio, 2018</span>
							<p>
								CANJA<br/>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								ARROZ COM BRÓCOLIS<br/>
								FEIJÃO S/CARNE<br/>
								MASSA AO MOLHO DE CARNE<br/>
								FETUCCINE FRESCO<br/>
								BATATA SUIÇA DE CALABRESA<br/>
								MOLHO DE TOMATES<br/>
								QUIBEBE DE ABOBORA <br/>
								BATATAS FRITAS <br/>
								BIFES À PARMEGIANA<br/>
								BIFES DE RÊS À DORÊ<br/>
								CARNE DE PANELA <br/>
								FRANGO <br/>
								FILÉ PEITO FRANGO GRELHADO<br/>
								FILÉ DE PEIXE GRELHADO<br/>
								ISCAS DE PEIXE À DORÊ<br/>
								SALSICHÃO<br/>
								ABOBRINHA ITALIANA<br/>
								RISOLES SABOR PIZZA<br/>
								PANQUECAS DE CARNE <br/>
								BOLINHOS DE ARROZ<br/>
								BUFFET DE SALADAS & DOCES
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Quarta-feira</h3>
							<span class="fh5co-event-meta">16 de maio, 2018</span>
							<p>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								ARROZ FATASIA<br/>
								FEIJÃO DE COR S/CARNES<br/>
								LASANHA À BOLOGNESA<br/>
								TALHARIM FRESCO CASEIRO<br/>
								MASSA COM GALINHA<br/>
								PIMENTÃO VERDE RECHEADO QUISADO/GRATINADO<br/>
								AIPIM<br/>
								BATATA DOCE ASSADA<br/>
								BANANAS EMPANADAS<br/>
								SUFLÊ DE ESPINAFRE<br/>
								ESTROGONOFF DE RÊS<br/>
								BIFES DE RÊS NA CHAPA ACEBOLADO<br/>
								SOBRE COXA DESOSSADA GRELHADA<br/>
								LINGUIÇINHA FINA CASTRO<br/>
								FRANGO ASSADO<br/>
								FILÉ DE PEIXE A DORE<br/>
								FILÉ DE PEIXE GRELHADO <br/>
								BOLINHO DE PEIXE<br/>
								PURÊ DE BATATA<br/>
								ABOBRINHA ITALIANA CALABRESA/QUEIJO<br/>
								TORTA FRIA<br/>
								PASTEL NAPOLITANO (presunto/queijo/tomate e azeitonas)<br/>
								POLENTA COM QUEIJO<br/>
								BUFFET DE SALADAS & DOCES
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Quinta-feira</h3>
							<span class="fh5co-event-meta">17 de maio, 2018</span>
							<p>
								FEIJOADA DO KOMILA'W<br/>
								FEIJÃO S/CARNES<br/>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								CARRETEIRO DE RÊS<br/>
								FETUCCINE FRESCO<br/>
								MASSA AO MOLHO BOLOGNESA<br/>
								LASANHA DE ESPINAFRE<br/>
								MOLHO DE TOMATES<br/>
								BATATA FRITA RÚSTICAS<br/>
								CENOURA REFOGADA<br/>
								COUVE À MINEIRA<br/>
								CALDINHO DE PIMENTA<br/>
								ABOBORA CARAMELADA<br/>
								POLENTA FRITA<br/>
								BOLINHO DE BATATAS DE CARNE<br/>
								ABOBRINHA ITALIANA<br/>
								CHUCRUTE<br/>
								TATU RECHEADO<br/>
								CHURRASCO DE RÊS<br/>
								GALETO NAS BRASAS <br/>
								COSTELINHA DE PORCO<br/>
								SALSICHÃO <br/>
								FILÉ DE PEIXE DORÊ<br/>
								FILÉ DE PEIXE GRELHADO <br/>
								FILÉ PEITO DE FRANGO GRELHADO<br/>
								BUFFET DE SALADAS E DOCES
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Sexta-feira</h3>
							<span class="fh5co-event-meta">18 de maio, 2018</span>
							<p>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								ARROZ COM BACALHAU<br/>
								FEIJÃO S/ CARNES<br/>
								MASSA AO MOLHO DE ATUM<br/>
								FETUCCINE FRESCO CASEIRO<br/>
								CAPELETE AO MOLHO TUCO<br/>
								REFOGADO DE SELETA DE LEGUMES<br/>
								BATATA FRITA <br/>
								PASTEL DE CAMARÃO<br/>
								MINI PASTEL DE QUEIJO<br/>
								ABOBRINHA ITALIANA GRELHADA E RECHEADA COM LEGUMES<br/>
								BATATA GRATINADA NA MANTEIGA<br/>
								ENTRECOT<br/>
								FILÉ PEITO DE FRANGO GRELHADO<br/>
								FRANGO AOS 4 QUEIJOS<br/>
								SALSICHÃO<br/>
								BACALHAU GOMES DE SÁ<br/>
								SALADA MAIONESE DE BACALHAU<br/>
								SALADA PORTUGUESA DE BACALHAU<br/>
								FILÉ DE PEIXE NA MANTEIGA NEGRA AO MOLHO DE ALCAPARRAS<br/>
								FILÉ DE PEIXE À DORÊ<br/>
								FILÉ DE TRAIRA<br/>
								PEIXE EM POSTA AO MOLHO ESCABECH<br/>
								FILÉ DE PEIXE GRELHADO<br/>
								LINGUIÇA DE PEIXE<br/>
								BOLINHO DE BACALHAU<br/>
								RISOLES DE CAMARÃO<br/>
								BUFFET DE DOCES & SALADAS
							</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="fh5co-event to-animate-2">
							<h3>Sábado</h3>
							<span class="fh5co-event-meta">19 de maio, 2018</span>
							<p>
								ARROZ BRANCO<br/>
								ARROZ INTEGRAL<br/>
								ARROZ À GREGA<br/>
								FEIJÃO S/CARNES<br/>
								ESPAGUETE FRESCO ALHO & OLEO<br/>
								FETUCCINE FRESCO CASEIR0<br/>
								LASANHA PRESUNTO/QUEIJO<br/>
								ABOBRINHA ITALIANA MARGUERITA<br/>
								AIPIM COZIDO COM FAROFA<br/>
								FAROFA <br/>
								BATATA FRITA <br/>
								BATATA DOCE ASSADA<br/>
								REFOGADO DE ERVILHAS FRESCAS COM OVOS<br/>
								BATATA AO 4 QUEIJOS<br/>
								CHURRASCO DE COSTELÃO DESOSSADO<br/>
								VAZIO<br/>
								CUPIM<br/>
								COPA/LOMBO SUINO AO MOLHO BARBECUE<br/>
								GALETO NAS BRASAS<br/>
								SALSICHÃO<br/>
								FILÉ DE PEIXE GRELHADO<br/>
								FILÉ DE LINGUADO À DORÊ <br/>
								CORDONBLEU PRESUNTO/QUEIJO<br/>
								CORAÇÃOZINHO <br/>
								POLENTA<br/>
								RISOLE DE FRANGO <br/>
								RISOLE DE QUEIJO<br/>
								ABACAXI ASSADO C/CANELA<br/>
								QUEIJO PROVOLONE<br/>
								BUFFET DE SALADAS & DOCES
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-sayings">
			<div class="container">
				<div class="row to-animate">

					<div class="flexslider">
						<ul class="slides">
							
							<li>
								<blockquote>
									<p>Com certeza o melhor restaurante da cidade, recomendo muito.</p>
									<p class="quote-author">&mdash; Luciana Braga</p>
								</blockquote>
							</li>
							<li>
								<blockquote>
									<p>Dificil pedir uma entrega e chegar quentinho, de todos da cidade que já fiz pedido Komilaw foi o único que me proporcionou isso sempre.&rdquo;</p>
									<p class="quote-author">&mdash; Rafaela Dobke</p>
								</blockquote>
							</li>
							<li>
								<blockquote>
									<p>&ldquo;Sexta-feira tem peixe, é uma delicía. &rdquo;</p>
									<p class="quote-author">&mdash; Julia Abraão</p>
								</blockquote>
							</li>
							<li>
								<blockquote>
									<p>&ldquo;Alaminuta de qualidade pedimos todos dias aqui na empresa até hoje não temos nenhuma crítica. Serviço de qualidade mesmo.&rdquo;</p>
									<p class="quote-author">&mdash; Felipe Coimbra</p>
								</blockquote>
							</li>
							
							
						</ul>
					</div>

				</div>
			</div>
		</div>

		<div id="fh5co-menus" data-section="menu">
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading to-animate">Nossa estrutura</h2>
						<p class="sub-heading to-animate">Dispomos de muita estrutura para você nos visitar..</p>
					</div>
				</div>
				<div class="row row-padded">
					<div class="col-md-3 text-center">
						<img src="images/estacionamento.png">
						<h3>Estacionamento próprio</h3>
					</div>		
					<div class="col-md-3 text-center">
						<img src="images/wifi.png">
						<h3>Wifi grátis</h3>
					</div>		
					<div class="col-md-3 text-center">
						<img src="images/delivery.png">
						<h3>Tele entrega</h3>
					</div>	
					<div class="col-md-3 text-center">
						<img src="images/climatizado.png">
						<h3>Ambiente climatizado</h3>
					</div>	
				</div>
			</div>
		</div>

		

		<div id="fh5co-contact" data-section="reservation">
			<div class="container">
				<div class="row text-center fh5co-heading row-padded">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="heading to-animate">Fale conosco</h2>
						<p class="sub-heading to-animate">Entre em contato conosco, será um prazer.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 to-animate-2">
						<h3>Endereços</h3>
						<ul class="fh5co-contact-info">
							<li class="fh5co-contact-address ">
								<i class="icon-home"></i>
								Rua Almirante Barroso, nº 2447 B
							</li>
							<li><i class="icon-phone"></i> (53) 3229.1066 - Buffet</li>
							<li><i class="icon-phone"></i> (53)3025-1066 - Pastelaria</li>
							<li><i class="icon-envelope"></i>contato@komilaw.com.br</li>
						</ul>
					</div>
					<div class="col-md-6 to-animate-2">
						<h3>Entre em contato.</h3>
						<div class="form-group ">
							<label for="name" class="sr-only">Seu nome</label>
							<input id="name" class="form-control" placeholder="Seu nome" type="text">
						</div>
						<div class="form-group ">
							<label for="email" class="sr-only">E-mail</label>
							<input id="email" class="form-control" placeholder="E-mail" type="email">
						</div>
						<div class="form-group">
							<label for="occation" class="sr-only">Assunto</label>
							<select class="form-control" id="occation">
								<option>Selecione</option>
							  <option>Crítica</option>
							  <option>Dúvida</option>
							  <option>Sugestão</option>
							</select>
						</div>		
						<div class="form-group ">
							<label for="message" class="sr-only">Sua mensagem</label>
							<textarea name="" id="message" cols="30" rows="5" class="form-control" placeholder="Digite sua mensagem"></textarea>
						</div>
						<div class="form-group ">
							<input class="btn btn-primary" value="Enviar sua mensagem" type="submit">
						</div>
						</div>
				</div>
			</div>
		</div>

		
	</div>

	<div id="fh5co-footer">
		<div class="container">
			<div class="row row-padded">
				<div class="col-md-12 text-center">
					<p class="to-animate">2018 Komil'aw Buffet e Pastelaria. <br> Desenvolvido por <a href="http://webhnet.com.br" target="_blank">Webhnet Oficina Web & Design</a>
					</p>
					<p class="text-center to-animate"><a href="#" class="js-gotop">Voltar ao topo</a></p>
				</div>
			</div>
		</div>
	</div>


	
	
	
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Bootstrap DateTimePicker -->
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script>
		$(function () {
	       $('#date').datetimepicker();
	   });
	</script>
	<!-- Main JS -->
	<script src="js/main.js"></script>

	</body>
</html>

