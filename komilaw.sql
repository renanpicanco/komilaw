-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 07-Jul-2018 às 19:04
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komilaw`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acrescimos`
--

CREATE TABLE `acrescimos` (
  `acrescimo_id` int(11) NOT NULL,
  `acrescimo_nome` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `acrescimo_valor_normal` decimal(7,2) NOT NULL,
  `acrescimo_valor_komilaw` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `acrescimos`
--

INSERT INTO `acrescimos` (`acrescimo_id`, `acrescimo_nome`, `acrescimo_valor_normal`, `acrescimo_valor_komilaw`) VALUES
(1, 'Azeitonas', '2.00', '4.00'),
(2, 'Bacon', '2.00', '4.00'),
(3, 'Catupiry', '2.00', '4.00'),
(4, 'Cebola', '1.00', '2.00'),
(5, 'Champignon', '5.00', '10.00'),
(6, 'Chedar', '2.00', '4.00'),
(7, 'Gorgonzola', '5.00', '10.00'),
(8, 'Milho', '1.00', '3.00'),
(9, 'Mussarela', '4.00', '9.00'),
(10, 'Ovo', '1.00', '1.00'),
(11, 'Palmito', '3.00', '6.00'),
(12, 'Presunto', '3.00', '6.00'),
(13, 'Tomate', '1.00', '2.00'),
(14, 'Tomate seco', '5.00', '10.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cardapio_frete`
--

CREATE TABLE `cardapio_frete` (
  `cardapio_frete_id` int(11) NOT NULL,
  `cardapio_frete_bairro` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `cardapio_frete_valor` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `cardapio_frete`
--

INSERT INTO `cardapio_frete` (`cardapio_frete_id`, `cardapio_frete_bairro`, `cardapio_frete_valor`) VALUES
(1, 'Fragata', '10.00'),
(2, 'Areal', '5.00'),
(3, 'Simões Lopes', '30.00'),
(4, 'Três Vendas', '30.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cardapio_pastelaria`
--

CREATE TABLE `cardapio_pastelaria` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `descricao` text,
  `preco_normal` decimal(7,2) NOT NULL,
  `preco_komilaw` decimal(7,2) DEFAULT NULL,
  `preco_aperitivo` decimal(7,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cardapio_pastelaria`
--

INSERT INTO `cardapio_pastelaria` (`id`, `cat_id`, `nome`, `descricao`, `preco_normal`, `preco_komilaw`, `preco_aperitivo`) VALUES
(1, 5, 'Refri Lata ', '', '4.00', '0.00', '0.00'),
(3, 5, 'Refri 2 LITROS', '', '8.00', '0.00', '0.00'),
(5, 5, 'Cerveja LATÃO', '', '5.00', '0.00', '0.00'),
(6, 5, 'Cerveja 600ML', '', '8.00', '0.00', '0.00'),
(7, 5, 'Cerveja EXTRA', '', '10.00', '0.00', '0.00'),
(8, 5, 'Taça vinho tinto', '', '4.00', '0.00', '0.00'),
(11, 5, 'H2O', '', '4.00', '0.00', '0.00'),
(12, 5, 'Água sem gás', '', '2.00', '0.00', '0.00'),
(13, 5, 'Guaraná caçula', '', '1.00', '0.00', '0.00'),
(14, 5, 'Água com gás', '', '2.00', '0.00', '0.00'),
(15, 3, 'Romeu e Julieta', 'Goiabada e queijo', '10.00', '0.00', '6.00'),
(16, 3, 'Chocolate preto com queijo', 'Chocolate preto e mussarela', '13.00', '0.00', '8.00'),
(17, 3, 'Banana com doce de leite', 'Banana, doce de leite e canela', '9.00', '0.00', '5.00'),
(18, 3, 'Chocolate preto', 'Chocolate preto ', '10.00', '0.00', '7.00'),
(19, 3, 'Chocolate branco', 'Chocolate branco ', '10.00', '0.00', '7.00'),
(20, 3, 'Choco MIX', 'Chocolate preto e branco ', '10.00', '0.00', '7.00'),
(21, 3, 'Maçã', 'Ma&ccedil;&atilde;, a&ccedil;&uacute;car e canela ', '9.00', '0.00', '5.00'),
(22, 3, 'Doce de leite', 'Doce de leite ', '8.00', '0.00', '5.00'),
(23, 3, 'Doce de leite com queijo', 'Doce de leite e mussarela ', '12.00', '0.00', '7.00'),
(24, 3, 'Choco preto e banana', 'chocolate preto e banana ', '11.00', '0.00', '7.00'),
(25, 3, 'Prestígio', 'Chocolate preto e coco ', '13.00', '0.00', '8.00'),
(26, 3, 'Banana', 'Banana, a&ccedil;ucar e canela ', '6.00', '0.00', '4.00'),
(27, 2, 'Camarão', 'Camar&atilde;o', '20.00', '38.00', '10.00'),
(28, 2, 'Camarão com catupiry', 'Camar&atilde;o e catupiry ', '22.00', '40.00', '0.00'),
(29, 2, 'Siri', 'Siri ', '14.00', '27.00', '0.00'),
(31, 2, 'Palmito com mussarela', 'Palmito e mussarela ', '11.00', '20.00', '0.00'),
(33, 2, '4 Queijos', 'Mussarela, provolone, catupiry e chedar', '15.00', '27.00', '0.00'),
(34, 2, '5 Queijos', 'Mussarela, provolone, catupiry, chedar e gorgonzola ', '18.00', '32.00', '0.00'),
(35, 2, 'Brócolis TURBO', 'Br&oacute;colis, catupiry e chedar ', '11.00', '20.00', '0.00'),
(36, 2, 'Milho', 'Milho verde, mussarela e or&eacute;gano', '11.00', '20.00', '0.00'),
(37, 2, 'Brocolis ', 'Br&oacute;colis e mussarela', '10.00', '18.00', '0.00'),
(38, 2, 'Vegetariano', 'Br&oacute;colis, tomate, milho, palmito e azeitonas', '9.00', '16.00', '0.00'),
(39, 2, 'Tomate seco', 'Tomate seco e mussarela ', '15.00', '27.00', '0.00'),
(40, 2, 'Filé com queijo', 'Fil&eacute; mignon e mussarela ', '20.00', '38.00', '0.00'),
(41, 2, 'Coração com queijo', 'Cora&ccedil;&atilde;o e mussarela ', '14.00', '26.00', '0.00'),
(42, 4, 'Croquete de carne', 'Carne ', '2.00', '0.00', '0.00'),
(46, 4, 'Fritas', '', '12.00', '0.00', '0.00'),
(47, 4, 'Risoles Frango ou Queijo', 'Por&ccedil;&atilde;o 10 unidades', '10.00', '0.00', '0.00'),
(48, 4, 'Porção de kibe', 'Por&ccedil;&atilde;o 05 unidades', '8.00', '0.00', '0.00'),
(49, 4, 'PORÇÃO DE CROQUETE ', 'Por&ccedil;&atilde;o 10 unidades ', '8.00', '0.00', '0.00'),
(50, 4, 'Enroladinho de salsicha', '', '4.00', '0.00', '0.00'),
(51, 4, 'Mini pastéis (unidade)', 'Frango, carne, calabresa, queijo ou Goiabada ', '2.00', '0.00', '0.00'),
(53, 1, 'Carne', 'Carne mo&iacute;da, azeitona, tempero verde e ovo', '9.00', '16.00', '5.00'),
(54, 1, 'Carne com queijo', 'Carne mo&iacute;da, azeitona e mussarela ', '13.00', '25.00', '7.00'),
(55, 1, 'Frango', 'Frango e Azeitonas ', '8.00', '15.00', '5.00'),
(56, 1, 'Frango com queijo', 'Frango, azeitonas e mussarela ', '12.00', '22.00', '7.00'),
(57, 1, 'Frango com catupiry', 'Frango, azeitonas e catupiry ', '10.00', '18.00', '6.00'),
(58, 1, 'Frango com catupiry e palmito', 'Frango, azeitonas, catupity e palmito ', '14.00', '26.00', '0.00'),
(59, 1, 'Napolitano', 'Presunto, mussarela, tomate, azeitonas e or&eacute;gano ', '12.00', '22.00', '0.00'),
(60, 1, 'Mussarela', 'Mussarela e or&eacute;gano ', '8.00', '15.00', '5.00'),
(61, 1, 'Calabresa', 'Calabresa, mussarela e or&eacute;gano ', '9.00', '16.00', '0.00'),
(62, 1, 'Presunto e queijo', 'Presunto, mussarela e or&eacute;gano', '9.00', '16.00', '5.00'),
(63, 1, 'Carne catupiry', 'carne moida, azeitonas e catupiry ', '11.00', '20.00', '6.00'),
(64, 1, 'Calabresa com chedar', 'calabresa, mussarela, chedar e oregano ', '11.00', '20.00', '0.00'),
(79, 4, 'FRITAS COM CHEEDAR E BACON', '', '17.00', '0.00', '0.00'),
(80, 5, 'COCA COLA 2 LTS', '', '9.00', '0.00', '0.00'),
(81, 5, 'MINI COCA', '', '2.00', '0.00', '0.00'),
(83, 2, 'STROGONOFF', 'Carne ao molho e Champignon.', '14.00', '24.00', '0.00'),
(84, 2, 'PORTUGUESA', 'Calbresa, mussarela, piment&atilde;o, ovo, azeitona e or&eacute;gano.', '12.00', '22.00', '0.00'),
(85, 2, 'Filé com Cheedar', 'Fil&eacute; Mignon, Mussarela e Cheedar.', '22.00', '42.00', '0.00'),
(86, 5, 'Refri 600 ml', '', '5.00', '0.00', '0.00'),
(88, 4, 'ISCAS DE CARNE COM FRITAS', '', '38.00', '0.00', '0.00'),
(89, 4, 'ISCAS DE FRANGO', '', '20.00', '0.00', '0.00'),
(90, 4, 'ISCAS DE PEIXE', '', '25.00', '0.00', '0.00'),
(91, 4, 'A LA MINUTA DE RÊS ', '', '18.00', '0.00', '0.00'),
(92, 4, 'A LA MINUTA DE FRANGO', '', '16.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cardapio_semanal`
--

CREATE TABLE `cardapio_semanal` (
  `cardapio_semanal_id` int(11) NOT NULL,
  `cardapio_semanal_dia` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `cardapio_semanal_data` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `cardapio_semanal_cardapio` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cardapio_semanal`
--

INSERT INTO `cardapio_semanal` (`cardapio_semanal_id`, `cardapio_semanal_dia`, `cardapio_semanal_data`, `cardapio_semanal_cardapio`) VALUES
(0, 'Segunda-feira', '14 de maio, 2018', '                                                                                                                                                                                                                           ARROZ BRANCO.\r\nARROZ INTEGRAL\r\nRISOTO DE FRANGO\r\nFEIJÃƒO S/CARNES\r\nTORTEI AO MOLHO VERMELHO\r\nFETTUCCINE FRESCO CASEIRO\r\nMASSA 4 QUEIJOS DE LEGUMES\r\nBATATA FRITA\r\nABOBRINHA ITALIANA\r\nMOLHO DE TOMATES\r\nQUISADINHO DE MILHO VERDE\r\nCROQUETINHO DE CARNE\r\nPASTEL DE FRANGO\r\nBATATAS PORTUGU?SA\r\nTATU(LAGARTO) AO MOLHO\r\nBIFES DE R?S\r\nFIL? PEITO FRANGO GRELHADO\r\nFIL? DE PEIXE GRELHADO\r\nISCAS DE PEIXE ? DOR?\r\nFRANGO\r\nSALSICH?O\r\nRISOLE DE QUEIJO\r\nBUFFET DE SALADAS & DOCES                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         '),
(1, 'Terça-feira', '15 de maio, 2018', 'CANJA\r\nARROZ BRANCO\r\nARROZ INTEGRAL\r\nARROZ COM BRÓCOLIS\r\nFEIJÃO S/CARNE\r\nMASSA AO MOLHO DE CARNE\r\nFETUCCINE FRESCO\r\nBATATA SUIÇA DE CALABRESA\r\nMOLHO DE TOMATES\r\nQUIBEBE DE ABOBORA\r\nBATATAS FRITAS\r\nBIFES À PARMEGIANA\r\nBIFES DE RÊS À DORÊ\r\nCARNE DE PANELA\r\nFRANGO\r\nFILÉ PEITO FRANGO GRELHADO\r\nFILÉ DE PEIXE GRELHADO\r\nISCAS DE PEIXE À DORÊ\r\nSALSICHÃO\r\nABOBRINHA ITALIANA\r\nRISOLES SABOR PIZZA\r\nPANQUECAS DE CARNE\r\nBOLINHOS DE ARROZ\r\nBUFFET DE SALADAS & DOCES '),
(2, 'Quarta-feira', '16 de maio, 2018', 'ARROZ BRANCO\r\nARROZ INTEGRAL\r\nARROZ FATASIA\r\nFEIJÃO DE COR S/CARNES\r\nLASANHA À BOLOGNESA\r\nTALHARIM FRESCO CASEIRO\r\nMASSA COM GALINHA\r\nPIMENTÃO VERDE RECHEADO QUISADO/GRATINADO\r\nAIPIM\r\nBATATA DOCE ASSADA\r\nBANANAS EMPANADAS\r\nSUFLÊ DE ESPINAFRE\r\nESTROGONOFF DE RÊS\r\nBIFES DE RÊS NA CHAPA ACEBOLADO\r\nSOBRE COXA DESOSSADA GRELHADA\r\nLINGUIÇINHA FINA CASTRO\r\nFRANGO ASSADO\r\nFILÉ DE PEIXE A DORE\r\nFILÉ DE PEIXE GRELHADO\r\nBOLINHO DE PEIXE\r\nPURÊ DE BATATA\r\nABOBRINHA ITALIANA CALABRESA/QUEIJO\r\nTORTA FRIA\r\nPASTEL NAPOLITANO (presunto/queijo/tomate e azeitonas)\r\nPOLENTA COM QUEIJO\r\nBUFFET DE SALADAS & DOCES '),
(3, 'Quinta-feira', '17 de maio, 2018', 'FEIJOADA DO KOMILA\'W\r\nFEIJÃO S/CARNES\r\nARROZ BRANCO\r\nARROZ INTEGRAL\r\nCARRETEIRO DE RÊS\r\nFETUCCINE FRESCO\r\nMASSA AO MOLHO BOLOGNESA\r\nLASANHA DE ESPINAFRE\r\nMOLHO DE TOMATES\r\nBATATA FRITA RÚSTICAS\r\nCENOURA REFOGADA\r\nCOUVE À MINEIRA\r\nCALDINHO DE PIMENTA\r\nABOBORA CARAMELADA\r\nPOLENTA FRITA\r\nBOLINHO DE BATATAS DE CARNE\r\nABOBRINHA ITALIANA\r\nCHUCRUTE\r\nTATU RECHEADO\r\nCHURRASCO DE RÊS\r\nGALETO NAS BRASAS\r\nCOSTELINHA DE PORCO\r\nSALSICHÃO\r\nFILÉ DE PEIXE DORÊ\r\nFILÉ DE PEIXE GRELHADO\r\nFILÉ PEITO DE FRANGO GRELHADO\r\nBUFFET DE SALADAS E DOCES '),
(4, 'Sexta-feira', '18 de maio, 2018', 'ARROZ BRANCO\r\nARROZ INTEGRAL\r\nARROZ COM BACALHAU\r\nFEIJÃO S/ CARNES\r\nMASSA AO MOLHO DE ATUM\r\nFETUCCINE FRESCO CASEIRO\r\nCAPELETE AO MOLHO TUCO\r\nREFOGADO DE SELETA DE LEGUMES\r\nBATATA FRITA\r\nPASTEL DE CAMARÃO\r\nMINI PASTEL DE QUEIJO\r\nABOBRINHA ITALIANA GRELHADA E RECHEADA COM LEGUMES\r\nBATATA GRATINADA NA MANTEIGA\r\nENTRECOT\r\nFILÉ PEITO DE FRANGO GRELHADO\r\nFRANGO AOS 4 QUEIJOS\r\nSALSICHÃO\r\nBACALHAU GOMES DE SÁ\r\nSALADA MAIONESE DE BACALHAU\r\nSALADA PORTUGUESA DE BACALHAU\r\nFILÉ DE PEIXE NA MANTEIGA NEGRA AO MOLHO DE ALCAPARRAS\r\nFILÉ DE PEIXE À DORÊ\r\nFILÉ DE TRAIRA\r\nPEIXE EM POSTA AO MOLHO ESCABECH\r\nFILÉ DE PEIXE GRELHADO\r\nLINGUIÇA DE PEIXE\r\nBOLINHO DE BACALHAU\r\nRISOLES DE CAMARÃO\r\nBUFFET DE DOCES & SALADAS '),
(5, 'Sábado', '19 de maio, 2018', 'ARROZ BRANCO\r\nARROZ INTEGRAL\r\nARROZ À GREGA\r\nFEIJÃO S/CARNES\r\nESPAGUETE FRESCO ALHO & OLEO\r\nFETUCCINE FRESCO CASEIR0\r\nLASANHA PRESUNTO/QUEIJO\r\nABOBRINHA ITALIANA MARGUERITA\r\nAIPIM COZIDO COM FAROFA\r\nFAROFA\r\nBATATA FRITA\r\nBATATA DOCE ASSADA\r\nREFOGADO DE ERVILHAS FRESCAS COM OVOS\r\nBATATA AO 4 QUEIJOS\r\nCHURRASCO DE COSTELÃO DESOSSADO\r\nVAZIO\r\nCUPIM\r\nCOPA/LOMBO SUINO AO MOLHO BARBECUE\r\nGALETO NAS BRASAS\r\nSALSICHÃO\r\nFILÉ DE PEIXE GRELHADO\r\nFILÉ DE LINGUADO À DORÊ\r\nCORDONBLEU PRESUNTO/QUEIJO\r\nCORAÇÃOZINHO\r\nPOLENTA\r\nRISOLE DE FRANGO\r\nRISOLE DE QUEIJO\r\nABACAXI ASSADO C/CANELA\r\nQUEIJO PROVOLONE\r\nBUFFET DE SALADAS & DOCES ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_produtos`
--

CREATE TABLE `categoria_produtos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_produtos`
--

INSERT INTO `categoria_produtos` (`id`, `nome`) VALUES
(1, 'Pastéis Tradicionais'),
(2, 'Pastéis Especiais'),
(3, 'Pastéis Doces'),
(4, 'Outros'),
(5, 'Bebidas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `con_acrescimo_produtos`
--

CREATE TABLE `con_acrescimo_produtos` (
  `con_id` int(11) NOT NULL,
  `con_acrescimo_id` int(11) NOT NULL,
  `con_produto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `pedido_id` int(11) NOT NULL,
  `pedido_nome` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `pedido_telefone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `pedido_email` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_pedido` text COLLATE latin1_general_ci NOT NULL,
  `pedido_valor` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_bairro` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_status` tinyint(1) NOT NULL DEFAULT '0',
  `pedido_endereco` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_cep` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_referencia_local` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pedido_data` date NOT NULL,
  `pedido_url_facebook` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acrescimos`
--
ALTER TABLE `acrescimos`
  ADD PRIMARY KEY (`acrescimo_id`);

--
-- Indexes for table `cardapio_frete`
--
ALTER TABLE `cardapio_frete`
  ADD PRIMARY KEY (`cardapio_frete_id`);

--
-- Indexes for table `cardapio_pastelaria`
--
ALTER TABLE `cardapio_pastelaria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `cardapio_semanal`
--
ALTER TABLE `cardapio_semanal`
  ADD PRIMARY KEY (`cardapio_semanal_id`);

--
-- Indexes for table `categoria_produtos`
--
ALTER TABLE `categoria_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `con_acrescimo_produtos`
--
ALTER TABLE `con_acrescimo_produtos`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`pedido_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acrescimos`
--
ALTER TABLE `acrescimos`
  MODIFY `acrescimo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cardapio_frete`
--
ALTER TABLE `cardapio_frete`
  MODIFY `cardapio_frete_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cardapio_pastelaria`
--
ALTER TABLE `cardapio_pastelaria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `categoria_produtos`
--
ALTER TABLE `categoria_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `con_acrescimo_produtos`
--
ALTER TABLE `con_acrescimo_produtos`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `pedido_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
